﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Threading;
using System.Windows.Forms;

namespace Panda
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        [DllImport("user32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        static extern bool SetForegroundWindow(IntPtr hWnd);
        [DllImport("user32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        private static extern bool ShowWindowAsync(IntPtr hWnd, int nCmdShow);
        static void Main(string[] args)
        {
            if (args.Length == 0)
            {
                Panda.Repository.BaseRepository.Show = false;
            }
            else
            {
                for (int i = 0; i < args.Length; i++) // Loop through array
                {
                    string argument = args[i];
                    if (i == 0)
                    {
                        if (argument.ToUpper() == "-NORMAL" || argument.ToUpper() == "-N")
                        {
                            Panda.Repository.BaseRepository.Show = true;
                        }
                        else if(argument.ToUpper() == "-KUNCI")
                        {
                            Panda.Repository.BaseRepository.Kunci();
                            Panda.Repository.BaseRepository.RunExplorer();
                        }
                        else if (argument.ToUpper() == "-BUKA")
                        {
                            Panda.Repository.BaseRepository.BukaKunci();
                            Panda.Repository.BaseRepository.RunExplorer();
                        }
                    }
                }
            }
            bool createdNew = true;
            using (Mutex mutex = new Mutex(true, "Panda", out createdNew))
            {
                if (createdNew)
                {
                    Application.EnableVisualStyles();
                    Application.SetCompatibleTextRenderingDefault(false);
                    Application.Run(new frmMain()); // replace [Form1] with your startup or main form.
                }
                else
                {
                    Panda.Repository.BaseRepository.Show = true;
                    Process currentProcess = Process.GetCurrentProcess();
                    foreach (Process process in Process.GetProcessesByName(currentProcess.ProcessName))
                    {
                        if (process.Id != currentProcess.Id)
                        {
                            SetForegroundWindow(process.MainWindowHandle);
                            // 1- to restore the window in it's normal status.
                            ShowWindowAsync(process.MainWindowHandle, 1);
                            break;
                        }
                    }
                }
            }
        }
    }
}

