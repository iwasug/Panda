﻿namespace Panda
{
    partial class frmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMain));
            this.bgWork1 = new System.ComponentModel.BackgroundWorker();
            this.bgWork2 = new System.ComponentModel.BackgroundWorker();
            this.notifyIcon1 = new System.Windows.Forms.NotifyIcon(this.components);
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.nsTheme1 = new NSTheme();
            this.nsControlButton2 = new NSControlButton();
            this.groupToko = new NSGroupBox();
            this.lblSocket = new System.Windows.Forms.Label();
            this.nsButton1 = new NSButton();
            this.lblStatus = new System.Windows.Forms.Label();
            this.pgBar = new NSProgressBar();
            this.lblLastUpdate = new NSLabel();
            this.nsControlButton1 = new NSControlButton();
            this.TimerTask = new System.Windows.Forms.Timer(this.components);
            this.nsTheme1.SuspendLayout();
            this.groupToko.SuspendLayout();
            this.SuspendLayout();
            // 
            // bgWork1
            // 
            this.bgWork1.WorkerReportsProgress = true;
            this.bgWork1.DoWork += new System.ComponentModel.DoWorkEventHandler(this.bgWork1_DoWork);
            this.bgWork1.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.bgWork1_ProgressChanged);
            this.bgWork1.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.bgWork1_RunWorkerCompleted);
            // 
            // bgWork2
            // 
            this.bgWork2.DoWork += new System.ComponentModel.DoWorkEventHandler(this.bgWork2_DoWork);
            // 
            // notifyIcon1
            // 
            this.notifyIcon1.Icon = ((System.Drawing.Icon)(resources.GetObject("notifyIcon1.Icon")));
            this.notifyIcon1.Text = "Panda";
            this.notifyIcon1.Visible = true;
            this.notifyIcon1.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.notifyIcon1_MouseDoubleClick);
            // 
            // timer1
            // 
            this.timer1.Interval = 3600000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // nsTheme1
            // 
            this.nsTheme1.AccentOffset = 0;
            this.nsTheme1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(50)))), ((int)(((byte)(50)))));
            this.nsTheme1.BorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.nsTheme1.Colors = new Bloom[0];
            this.nsTheme1.Controls.Add(this.nsControlButton2);
            this.nsTheme1.Controls.Add(this.groupToko);
            this.nsTheme1.Controls.Add(this.nsControlButton1);
            this.nsTheme1.Customization = "";
            this.nsTheme1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.nsTheme1.Font = new System.Drawing.Font("Verdana", 8F);
            this.nsTheme1.Image = null;
            this.nsTheme1.Location = new System.Drawing.Point(0, 0);
            this.nsTheme1.Movable = true;
            this.nsTheme1.Name = "nsTheme1";
            this.nsTheme1.NoRounding = true;
            this.nsTheme1.Padding = new System.Windows.Forms.Padding(10, 40, 10, 10);
            this.nsTheme1.Sizable = false;
            this.nsTheme1.Size = new System.Drawing.Size(444, 187);
            this.nsTheme1.SmartBounds = true;
            this.nsTheme1.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.nsTheme1.TabIndex = 0;
            this.nsTheme1.Text = "-";
            this.nsTheme1.TransparencyKey = System.Drawing.Color.Empty;
            this.nsTheme1.Transparent = false;
            // 
            // nsControlButton2
            // 
            this.nsControlButton2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.nsControlButton2.ControlButton = NSControlButton.Button.Minimize;
            this.nsControlButton2.Location = new System.Drawing.Point(399, 4);
            this.nsControlButton2.Margin = new System.Windows.Forms.Padding(0);
            this.nsControlButton2.MaximumSize = new System.Drawing.Size(18, 20);
            this.nsControlButton2.MinimumSize = new System.Drawing.Size(18, 20);
            this.nsControlButton2.Name = "nsControlButton2";
            this.nsControlButton2.Size = new System.Drawing.Size(18, 20);
            this.nsControlButton2.TabIndex = 2;
            this.nsControlButton2.Text = "nsControlButton2";
            // 
            // groupToko
            // 
            this.groupToko.Controls.Add(this.lblSocket);
            this.groupToko.Controls.Add(this.nsButton1);
            this.groupToko.Controls.Add(this.lblStatus);
            this.groupToko.Controls.Add(this.pgBar);
            this.groupToko.Controls.Add(this.lblLastUpdate);
            this.groupToko.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupToko.DrawSeperator = false;
            this.groupToko.ForeColor = System.Drawing.Color.White;
            this.groupToko.Location = new System.Drawing.Point(10, 40);
            this.groupToko.Name = "groupToko";
            this.groupToko.Padding = new System.Windows.Forms.Padding(10, 40, 10, 10);
            this.groupToko.Size = new System.Drawing.Size(424, 137);
            this.groupToko.SubTitle = "-";
            this.groupToko.TabIndex = 1;
            this.groupToko.Text = "nsGroupBox1";
            this.groupToko.Title = "-";
            // 
            // lblSocket
            // 
            this.lblSocket.AutoSize = true;
            this.lblSocket.Location = new System.Drawing.Point(13, 114);
            this.lblSocket.Name = "lblSocket";
            this.lblSocket.Size = new System.Drawing.Size(12, 13);
            this.lblSocket.TabIndex = 5;
            this.lblSocket.Text = "-";
            // 
            // nsButton1
            // 
            this.nsButton1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.nsButton1.Location = new System.Drawing.Point(318, 86);
            this.nsButton1.Name = "nsButton1";
            this.nsButton1.Size = new System.Drawing.Size(97, 38);
            this.nsButton1.TabIndex = 4;
            this.nsButton1.Text = "UnBlock";
            this.nsButton1.Click += new System.EventHandler(this.nsButton1_Click);
            // 
            // lblStatus
            // 
            this.lblStatus.AutoSize = true;
            this.lblStatus.Font = new System.Drawing.Font("Verdana", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblStatus.Location = new System.Drawing.Point(14, 73);
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.Size = new System.Drawing.Size(9, 10);
            this.lblStatus.TabIndex = 3;
            this.lblStatus.Text = "-";
            // 
            // pgBar
            // 
            this.pgBar.Location = new System.Drawing.Point(14, 86);
            this.pgBar.Maximum = 100;
            this.pgBar.Minimum = 0;
            this.pgBar.Name = "pgBar";
            this.pgBar.Size = new System.Drawing.Size(298, 24);
            this.pgBar.TabIndex = 1;
            this.pgBar.Text = "nsProgressBar1";
            this.pgBar.Value = 0;
            // 
            // lblLastUpdate
            // 
            this.lblLastUpdate.Font = new System.Drawing.Font("Verdana", 10.25F, System.Drawing.FontStyle.Bold);
            this.lblLastUpdate.Location = new System.Drawing.Point(13, 43);
            this.lblLastUpdate.Name = "lblLastUpdate";
            this.lblLastUpdate.Size = new System.Drawing.Size(299, 30);
            this.lblLastUpdate.TabIndex = 6;
            this.lblLastUpdate.Text = "nsLabel2";
            this.lblLastUpdate.Value1 = "-";
            this.lblLastUpdate.Value2 = "-";
            // 
            // nsControlButton1
            // 
            this.nsControlButton1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.nsControlButton1.ControlButton = NSControlButton.Button.Close;
            this.nsControlButton1.Location = new System.Drawing.Point(420, 4);
            this.nsControlButton1.Margin = new System.Windows.Forms.Padding(0);
            this.nsControlButton1.MaximumSize = new System.Drawing.Size(18, 20);
            this.nsControlButton1.MinimumSize = new System.Drawing.Size(18, 20);
            this.nsControlButton1.Name = "nsControlButton1";
            this.nsControlButton1.Size = new System.Drawing.Size(18, 20);
            this.nsControlButton1.TabIndex = 0;
            this.nsControlButton1.Text = "nsControlButton1";
            // 
            // TimerTask
            // 
            this.TimerTask.Tick += new System.EventHandler(this.TimerTask_Tick);
            // 
            // frmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(444, 187);
            this.Controls.Add(this.nsTheme1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmMain";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "frmMain";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmMain_FormClosing);
            this.Load += new System.EventHandler(this.frmMain_Load);
            this.Resize += new System.EventHandler(this.frmMain_Resize);
            this.nsTheme1.ResumeLayout(false);
            this.groupToko.ResumeLayout(false);
            this.groupToko.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private NSTheme nsTheme1;
        private NSControlButton nsControlButton1;
        private NSGroupBox groupToko;
        private System.ComponentModel.BackgroundWorker bgWork1;
        private NSProgressBar pgBar;
        private System.Windows.Forms.Label lblStatus;
        private NSControlButton nsControlButton2;
        private NSButton nsButton1;
        private System.ComponentModel.BackgroundWorker bgWork2;
        private System.Windows.Forms.Label lblSocket;
        private System.Windows.Forms.NotifyIcon notifyIcon1;
        private NSLabel lblLastUpdate;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Timer TimerTask;
    }
}