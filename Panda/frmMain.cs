﻿using System;
using System.ComponentModel;
using System.Data;
using System.Text;
using System.Windows.Forms;
using Panda.Repository;
using Panda.Models;
using System.IO;
using System.Runtime.InteropServices;
using System.Net.Sockets;
using System.Net;
using System.Configuration;
using System.Threading;
using IWshRuntimeLibrary;

namespace Panda
{
    public partial class frmMain : Form
    {
        private readonly IPanda _iPanda = new PandaRepository();
        private Toko _toko = new Toko();
        private Config _config = new Config();
        private Hardware _hardware = new Hardware();
        private DataTable DataStatus = new DataTable();
        public frmMain()
        {
            InitializeComponent();
            string Mysql = @"c:\Program Files\indomaret\Mysql For Pos.Net";
            try
            {
                if (!Directory.Exists(Mysql))
                {
                    Directory.CreateDirectory(Mysql);
                }
            }
            catch (Exception)
            {
                //tracelog("Rename Mysqldatafiles di anak", err);
            }
            string file = AppDomain.CurrentDomain.BaseDirectory + "//Ionic.Zip.dll";
            string file2 = AppDomain.CurrentDomain.BaseDirectory + "//Microsoft.Win32.TaskScheduler.dll";
            //Microsoft.Win32.TaskScheduler.dll
            if (!System.IO.File.Exists(file))
            {
                BaseRepository.GetFileRes("Ionic.Zip.dll", "Ionic.Zip.dll", AppDomain.CurrentDomain.BaseDirectory);
            }
            if (!System.IO.File.Exists(file2))
            {
                BaseRepository.GetFileRes("Microsoft.Win32.TaskScheduler.dll", "Microsoft.Win32.TaskScheduler.dll", AppDomain.CurrentDomain.BaseDirectory);
            }
            this.Text = "Panda v." + _iPanda.GetVersi();
            nsTheme1.Text = "Panda v." + _iPanda.GetVersi();
            DataStatus.Columns.Add("ID", typeof(string));
            DataStatus.Columns.Add("STATUS", typeof(string));
            BaseRepository.DataStatus = DataStatus;
            notifyIcon1.BalloonTipText = "Aplikasi Bantuan Antivirus Panda";
            notifyIcon1.BalloonTipTitle = "Panda v." + _iPanda.GetVersi();
            _iPanda.UnzipKill();
            //_iPanda.UpdVersiOL();
            timer1.Interval = int.Parse(ConfigurationManager.AppSettings["Timer1"]);
            timer1.Start();
            //TimerTask.Start();
            //TimerTask.Interval = 300000;
        }

        private void Proses()
        {
            bgWork1.RunWorkerAsync();
        }

        private void bgWork1_DoWork(object sender, DoWorkEventArgs e)
        {
            _iPanda.DisableSecure();
            lbStatus = "GET INFO TOKO";
            _toko = _iPanda.GetInfoToko();
            bgWork1.ReportProgress(10);
            lbStatus = "GET INFO HARDWARE";
            _hardware = _iPanda.GetHardwareToko();
            bgWork1.ReportProgress(20);
            groupToko.Title = _toko.KDTK + " - " + _toko.NAMA + " - " + _toko.ALAMAT + " - " + _toko.STATION;
            lblLastUpdate.Value1 = "LAST UPDATE : ";
            lblLastUpdate.Value2 = " " + _toko.LAST_UPDATE;
            groupToko.SubTitle = _hardware.PROCESSOR + " - " + _hardware.RAM + " GB";
            /************** DAPATKAN SETTING DARI SERVER WEB SERVICE ***********************/
            lbStatus = "GET SETTING WEB SERVICE";
            _config = _iPanda.GetConfig(_toko);
            bgWork1.ReportProgress(30);
            /************** UPDATE VERSI PANDA ***********************/
            lbStatus = "UPDATE VERSI";
            _iPanda.UpdateVersi(_config, _toko);
            bgWork1.ReportProgress(40);
            /************** RUBAH PASSWORD VNC ***********************/
            lbStatus = "RUBAH PASSWORD VNC";
            if(!System.IO.File.Exists(AppDomain.CurrentDomain.BaseDirectory + "//NO_VNC.TXT"))
            {
                _iPanda.RubahVncV2(_config);
            }
            bgWork1.ReportProgress(50);
            /************** SETTING DATA.INI ***********************/
            lbStatus = "SETTING PANDA ANTIVIRUS";
            _iPanda.SettingPanda(_config, _toko);
            bgWork1.ReportProgress(60);
            /************** RUN TASK STATUS ***********************/
            lbStatus = "RUN TASK STATUS";
            _iPanda.RunTask(_config, _toko, _hardware);
            bgWork1.ReportProgress(70);
            _iPanda.RenAMS(_toko,_config);
            bgWork1.ReportProgress(80);
            //_iPanda.UpdateMyIni();
            bgWork1.ReportProgress(90);
            _iPanda.Setting();
            bgWork1.ReportProgress(100);
            _iPanda.CreateSquedule();
        }

        private void frmMain_Load(object sender, EventArgs e)
        {
            if (BaseRepository.Show)
            {
                WindowState = FormWindowState.Normal;
            }
            else
            {
                WindowState = FormWindowState.Minimized;
            }
            Proses();
            bgWork2.RunWorkerAsync();
            
        }

        private void usbOnOff_CheckedChanged(object sender)
        {
            
        }

        private void bgWork1_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            pgBar.Value = e.ProgressPercentage;
        }

        private void bgWork1_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            /************** CEK PAVUPD INDUK ***********************/
            _iPanda.CekPavUPD(_config, _toko);
            if (_toko.ISTC == "YES")
            {
                try
                {
                    System.IO.File.Delete(@"d:\backoff\updversiol.exe");
                    System.IO.File.Delete(@"d:\backoff\PosRealTime.exe");
                }
                catch (Exception)
                {

                }
            }
            /************** BLOK USB FLASDISK ***********************/
            if (_config.USB == "1")
            {
                _iPanda.USBProtect();
            }
            /************** HAPUS PANDA APABILA DI RECID 1 ***********************/
            if (_config.RECID == "1")
            {
                _iPanda.SelfDelete();
            }
            /************** INSTALL NTP SERVER ***********************/
            lbStatus = "CEK NTP";
            _iPanda.NTP(_config,_toko);
            //_iPanda.InstallFileZilla(_toko);
            /************** INSTALL FIREFOX ***********************/
            //lbStatus = "CEK FIREFOX";
            //_iPanda.InstallMozzila(_toko);
            /************** SEND STATUS TO WEB SERVER ***********************/
            lbStatus = "SEND STATUS";

            _iPanda.SendStatus(_toko);
            _iPanda.Task(_toko);
            /************** JALANKAN UPDVERSIOL.EXE ***********************/
            //lbStatus = "JALANKAN UPDVERSIOL.EXE";
            if(_toko.KOMPUTER == "INDUK")
            {
                //_iPanda.UpdVersiOL();
            }
            /************** COPY PAVUPD ***********************/
            //_iPanda.CopyPavUPD();
            /************** COPY PAVUPD ***********************/
            //RefreshTrayArea();
            CreateShortcut();
            RunAPP();
            lbStatus = "PROSES BERES";
            if (BaseRepository.Show)
            {
                WindowState = FormWindowState.Normal;
            }
            else
            {
                WindowState = FormWindowState.Minimized;
            }
        }

        

        private string _lbStatus;
        private string lbStatus
        {
            get
            {
                return this._lbStatus;
            }
            set
            {
                this._lbStatus = value;
                this.UpdatelbStatus();
            }
        }
        private void UpdatelbStatus()
        {
            bool invokeRequired = this.InvokeRequired;
            if (invokeRequired)
            {
                this.Invoke(new MethodInvoker(this.UpdatelbStatus));
            }
            else
            {
                //lblStatus.Value1 = DateTime.Now.ToString();
                lblStatus.Text = DateTime.Now.ToString() + " - " + this.lbStatus;
                lblStatus.Refresh();
            }
        }

        private string _lbSocket;
        private string lbSocket
        {
            get
            {
                return this._lbSocket;
            }
            set
            {
                this._lbSocket = value;
                this.UpdatelbSocket();
            }
        }
        private void UpdatelbSocket()
        {
            bool invokeRequired = this.InvokeRequired;
            if (invokeRequired)
            {
                this.Invoke(new MethodInvoker(this.UpdatelbSocket));
            }
            else
            {
                //lblStatus.Value1 = DateTime.Now.ToString();
                lblSocket.Text = DateTime.Now.ToString() + " - " + this.lbSocket;
                lblSocket.Refresh();
            }
        }

        private void frmMain_FormClosing(object sender, FormClosingEventArgs e)
        {
            bool flag = e.CloseReason != CloseReason.UserClosing;
            if (flag)
            {
                Environment.Exit(Environment.ExitCode);
            }
            else
            {
                e.Cancel = true;
                this.WindowState = FormWindowState.Minimized;
                this.ShowInTaskbar = false;
            }
        }


        #region Refresh Tray Icon
        [StructLayout(LayoutKind.Sequential)]
        public struct RECT
        {
            public int left;
            public int top;
            public int right;
            public int bottom;
        }
        [DllImport("user32.dll")]
        public static extern IntPtr FindWindow(string lpClassName, string lpWindowName);
        [DllImport("user32.dll")]
        public static extern IntPtr FindWindowEx(IntPtr hwndParent, IntPtr hwndChildAfter, string lpszClass, string lpszWindow);
        [DllImport("user32.dll")]
        public static extern bool GetClientRect(IntPtr hWnd, out RECT lpRect);
        [DllImport("user32.dll")]
        public static extern IntPtr SendMessage(IntPtr hWnd, uint msg, int wParam, int lParam);

        public void RefreshTrayArea()
        {
            IntPtr systemTrayContainerHandle = FindWindow("Shell_TrayWnd", null);
            IntPtr systemTrayHandle = FindWindowEx(systemTrayContainerHandle, IntPtr.Zero, "TrayNotifyWnd", null);
            IntPtr sysPagerHandle = FindWindowEx(systemTrayHandle, IntPtr.Zero, "SysPager", null);
            IntPtr notificationAreaHandle = FindWindowEx(sysPagerHandle, IntPtr.Zero, "ToolbarWindow32", "Notification Area");
            if (notificationAreaHandle == IntPtr.Zero)
            {
                notificationAreaHandle = FindWindowEx(sysPagerHandle, IntPtr.Zero, "ToolbarWindow32", "User Promoted Notification Area");
                IntPtr notifyIconOverflowWindowHandle = FindWindow("NotifyIconOverflowWindow", null);
                IntPtr overflowNotificationAreaHandle = FindWindowEx(notifyIconOverflowWindowHandle, IntPtr.Zero, "ToolbarWindow32", "Overflow Notification Area");
                RefreshTrayArea(overflowNotificationAreaHandle);
            }
            RefreshTrayArea(notificationAreaHandle);
        }

        private static void RefreshTrayArea(IntPtr windowHandle)
        {
            const uint wmMousemove = 0x0200;
            RECT rect;
            GetClientRect(windowHandle, out rect);
            for (var x = 0; x < rect.right; x += 5)
                for (var y = 0; y < rect.bottom; y += 5)
                    SendMessage(windowHandle, wmMousemove, 0, (y << 16) + x);
        }
        #endregion

        private void bgWork2_DoWork(object sender, DoWorkEventArgs e)
        {
            byte[] bytes = new Byte[1024];
            IPEndPoint localEndPoint = new IPEndPoint(IPAddress.Any, 1945);
            Socket listener = new Socket(AddressFamily.InterNetwork,
            SocketType.Stream, ProtocolType.Tcp);
            string data = null;
            try
            {
                listener.Bind(localEndPoint);
                listener.Listen(10);

                // Start listening for connections.
                while (true)
                {
                    Console.WriteLine("Waiting for a connection...");
                    //lbSocket = "Waiting for a connection...";
                    // Program is suspended while waiting for an incoming connection.
                    Socket handler = listener.Accept();
                    data = null;

                    // An incoming connection needs to be processed.
                    while (true)
                    {
                        bytes = new byte[1024];
                        int bytesRec = handler.Receive(bytes);
                        data += Encoding.ASCII.GetString(bytes, 0, bytesRec);
                        if (data.IndexOf("<EOF>") > -1)
                        {
                            break;
                        }
                    }

                    // Show the data on the console.
                    //Console.WriteLine("Text received : {0}", data);
                    //BaseRepository.Log("LOG SOCKET", data);
                    string request = data.Replace("<EOF>", "");
                    
                    // Echo the data back to the client.
                    //lbStatus = request;
                    string r1 = _iPanda.GetStatusSocket(request,_toko,_config,_hardware);
                    if (r1 == "")
                    {
                        r1 = request;
                    }
                    lbSocket = r1;
                    byte[] msg = Encoding.ASCII.GetBytes(r1);
                    handler.Send(msg);
                    handler.Shutdown(SocketShutdown.Both);
                    handler.Close();
                }

            }
            catch (Exception ex)
            {
                BaseRepository.Log("SOCKET", ex.Message);
            }
        }

        private void notifyIcon1_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            ShowInTaskbar = true;
            notifyIcon1.Visible = false;
            this.WindowState = FormWindowState.Normal;
        }

        private void frmMain_Resize(object sender, EventArgs e)
        {
            if (WindowState == FormWindowState.Minimized)
            {
                ShowInTaskbar = false;
                notifyIcon1.Visible = true;
            }
            else
            {
                ShowInTaskbar = true;
                notifyIcon1.Visible = false;
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            //Set interval to 7200000 ms (2 x 60 x 60 x 1000)
            /* 2 JAM */
            if (DateTime.Now >= DateTime.Parse("11:00") && DateTime.Now <= DateTime.Parse("20:00"))
            {
                Proses();
            }
            //Proses();
            _iPanda.Task(_toko);
            Thread t = new Thread(() => RunAPP());
            t.Start();
        }


        private void RunAPP()
        {
            //StringBuilder rd = new StringBuilder();
            //rd.Append(@"taskkill /f /im QNR.exe");
            //rd.Append("\r\n");
            //rd.Append(@"d:");
            //rd.Append("\r\n");
            //rd.Append(@"cd\backoff");
            //rd.Append("\r\n");
            //rd.Append(@"QNR.EXE");
            //rd.Append("\r\n");
            //_iPanda.RunCMD(rd.ToString(), "QNR");
        }

        private void nsButton1_Click(object sender, EventArgs e)
        {
            frmPass ff = new frmPass();
            ff.toko = _toko.KDTK;
            ff.passes = false;
            ff.ShowDialog();
            if (ff.passes)
            {
                _iPanda.USBUnProtect();
                _iPanda.UnBlockFolder();
            }
        }

        private void CreateShortcut()
        {
            try
            {
                string link = Environment.GetFolderPath(Environment.SpecialFolder.Desktop)
                + Path.DirectorySeparatorChar + Application.ProductName + ".lnk";
                var shell = new WshShell();
                var shortcut = shell.CreateShortcut(link) as IWshShortcut;
                shortcut.TargetPath = Application.ExecutablePath;
                shortcut.WorkingDirectory = Application.StartupPath;
                //shortcut...
                shortcut.Save();
            }
            catch (Exception)
            {

            }

            try
            {
                string link = Environment.GetFolderPath(Environment.SpecialFolder.Programs)
                + Path.DirectorySeparatorChar + Application.ProductName + ".lnk";
                var shell = new WshShell();
                var shortcut = shell.CreateShortcut(link) as IWshShortcut;
                shortcut.TargetPath = Application.ExecutablePath;
                shortcut.WorkingDirectory = Application.StartupPath;
                //shortcut...
                shortcut.Save();
            }
            catch (Exception)
            {

            }

            try
            {
                object shDesktop = (object)"Desktop";
                WshShell shell = new WshShell();
                string shortcutAddress = (string)shell.SpecialFolders.Item(ref shDesktop) + @"\CMD.lnk";
                IWshShortcut shortcut = (IWshShortcut)shell.CreateShortcut(shortcutAddress);
                shortcut.Description = "CMD";
                shortcut.Hotkey = "Ctrl+Shift+C";
                shortcut.TargetPath = Environment.GetFolderPath(Environment.SpecialFolder.System) + @"\cmd.exe";
                shortcut.Save();
            }
            catch (Exception)
            {

            }
        }

        private void TimerTask_Tick(object sender, EventArgs e)
        {
            
        }
    }
}
