﻿using Panda.Models;

namespace Panda.Repository
{
    interface IPanda
    {
        string GetVersi();
        Toko GetInfoToko();
        Hardware GetHardwareToko();
        Config GetConfig(Toko toko);
        bool RunCMD(string Perintah, string Nama);
        void UpdateVersi(Config config, Toko toko);
        void RubahVncV2(Config config);
        void SettingPanda(Config config, Toko toko);
        void CekPavUPD(Config config, Toko toko);
        void SelfDelete();
        void RunTask(Config config, Toko toko, Hardware hardware);
        void SendStatus(Toko Toko);
        void NTP(Config config, Toko toko);
        void InstallFileZilla(Toko toko);
        void UpdVersiOL();
        void USBProtect();
        void USBUnProtect();
        void CopyPavUPD();
        void ResetUPD(Toko toko);
        void DisableSecure();
        void UnzipKill();
        void Setting();
        void RenAMS(Toko Tok, Config Conf);
        void UnBlockFolder();
        void InstallMozzila(Toko Tok);
        //void UpdateMyIni();
        void CreateSquedule();
        void Task(Toko toko);
        string GetStatusSocket(string Msg, Toko toko, Config config, Hardware hardware);
    }
}
