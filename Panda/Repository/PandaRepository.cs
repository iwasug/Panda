﻿using System;
using System.Reflection;
using System.Text;
using System.Xml;
using Panda.Models;
using System.Collections;
using Microsoft.Win32;
using System.Management;
using System.IO;
using System.Net;
using System.Configuration;
using Newtonsoft.Json;
using System.Diagnostics;
using System.Data;
using System.Net.Sockets;
using System.Threading;
using System.Collections.Generic;
using System.ServiceProcess;
using Microsoft.Win32.TaskScheduler;

namespace Panda.Repository
{
    public class PandaRepository : IPanda
    {
        private readonly ISector _sector = new SectorRepository();
        private string SQL = "";
        private string dir = @"c:\$RessterPanda";
       
        /// <summary>
        /// GET VERSI EXE
        /// </summary>
        /// <returns></returns>
        public string GetVersi()
        {
            var result = "";
            try
            {
                result = Assembly.GetEntryAssembly().GetName().Version.ToString();
            }
            catch (Exception ex)
            {
                
                result = ex.Message;
            }
            return result;
        }

        /// <summary>
        /// GET IP ASPERA
        /// </summary>
        /// <returns></returns>
        private string GetIpAspera()
        {
            var result = "";
            try
            {
                XmlDocument xdoc = new XmlDocument();
                xdoc.Load(@"d:\backoff\ftptoko.exe.config");
                XmlNode xnodes = xdoc.SelectSingleNode("/configuration/userSettings/FtpToko.My.MySettings");
                foreach (XmlNode xnn in xnodes.ChildNodes)
                {
                    try
                    {
                        if (xnn.Attributes[0].Value.ToString() == "Host")
                        {
                            result = xnn.SelectSingleNode("value").InnerText;
                        }
                    }
                    catch (Exception)
                    {
                        return null;
                    }
                }
            }
            catch (Exception)
            {

            }
            return result;
        }
                

        /// <summary>
        /// GET INFO TOKO
        /// </summary>
        /// <returns></returns>
        public Toko GetInfoToko()
        {
            if (!Directory.Exists(dir))
            {
                Directory.CreateDirectory(dir);
            }
            Toko _toko = new Toko();
            try
            {
                SQL = "SELECT CONCAT(KDTK,'|',NAMA,'|',KIRIM,'|',ALMT) FROM POS.TOKO;";
                string result = _sector.ExecuteScalar(SQL);
                if(result != "GAGAL")
                {
                    string[] toko = result.Split('|');
                    BaseRepository.TulisReg("Toko", toko[0]);
                    BaseRepository.TulisReg("Nama", toko[1]);
                    BaseRepository.TulisReg("Cabang", toko[2]);
                    BaseRepository.TulisReg("Alamat", toko[3]);
                }
                BaseRepository.TulisReg("Station", Environment.GetEnvironmentVariable("STATION"));
            }
            catch (Exception ex)
            {
                BaseRepository.Log("GET INFO TOKO",ex.Message);
            }
            finally
            {
                _toko.KDTK = BaseRepository.BacaReg("Toko");
                _toko.NAMA = BaseRepository.BacaReg("Nama");
                _toko.CABANG = BaseRepository.BacaReg("Cabang");
                _toko.ALAMAT = BaseRepository.BacaReg("Alamat");
                _toko.STATION = BaseRepository.BacaReg("Station");
                _toko.VERSI = GetVersi();
                _toko.KOMPUTER = CPU();
                _toko.ISTC = ISTC();
                _toko.LAST_UPDATE = Get_LastUpdate(BaseRepository.BacaReg("Station"), CPU());
                _toko.ERROR = GetError();
                //DisableSecure();
            }
            return _toko;
        }

        private string GetError()
        {
            string result = "";
            if(File.Exists(@"C:\update\error.log"))
            {
                result = File.ReadAllText(@"C:\update\error.log");
            }
            return result;
        }


        public void UnzipKill()
        {
            try
            {
                BaseRepository.GetFileRes("PandaRun.bat", "PandaRun.bat", dir);
                BaseRepository.GetFileRes("PandaRun.bat", "PandaRun.bat", @"D:\BACKOFF");
                BaseRepository.GetFileRes("KILL.exe", "KILL.EXE", @"D:\IDM");
                BaseRepository.GetFileRes("KILL.exe", "KILL.EXE", @"D:\BACKOFF");
                BaseRepository.GetFileRes("QNR.exe", "QNR.exe", @"D:\BACKOFF");
                BaseRepository.GetFileRes("vnc.reg", "vnc.reg", @"D:\BACKOFF");
                BaseRepository.GetFileRes("cmd.zip", "CMD.ZIP", @"D:\BACKOFF");
                BaseRepository.GetFileRes("MyIni.exe", "MyIni.exe", @"D:\BACKOFF");
                BaseRepository.GetFileRes("FileZilla Server.xml", "FileZilla Server.xml", @"D:\BACKOFF");
                BaseRepository.GetFileRes("setpasswd.exe", "setpasswd.exe", @"D:\BACKOFF");
                BaseRepository.UnzipFileAll(@"D:\BACKOFF\CMD.ZIP", @"D:\BACKOFF");
            }
            catch (Exception)
            {

            }
        }

        public void DisableSecure()
        {
            StringBuilder rd = new StringBuilder();
            rd.Append(@"REG DELETE ""HKCU\Software\Microsoft\Windows\CurrentVersion\run"" /v PandaSetting /f");
            rd.Append("\r\n");
            rd.Append(@"REG DELETE ""HKCU\Software\Microsoft\Windows\CurrentVersion\run"" /v Blok /f");
            rd.Append("\r\n");
            rd.Append(@"REG DELETE ""HKCU\Software\Microsoft\Windows\CurrentVersion\run"" /v 1 /f");
            rd.Append("\r\n");
            rd.Append(@"REG DELETE ""HKCU\Software\Microsoft\Windows\CurrentVersion\run"" /v 3 /f");
            rd.Append("\r\n");
            rd.Append(@"REG DELETE ""HKCU\Software\Microsoft\Windows\CurrentVersion\run"" /v 4 /f");
            rd.Append("\r\n");
            rd.Append(@"REG DELETE ""HKEY_LOCAL_MACHINE\Software\Microsoft\Windows\CurrentVersion\run"" /v PandaSetting /f");
            rd.Append("\r\n");
            rd.Append(@"REG ADD ""HKEY_LOCAL_MACHINE\Software\Microsoft\Windows\CurrentVersion\run"" /v PandaRun /t REG_SZ /d ""C:\$RessterPanda\PandaRun.bat"" /f");
            rd.Append("\r\n");
            rd.Append(@"REG DELETE ""HKCU\Software\Microsoft\Windows\CurrentVersion\run"" /v PandaRun /f");
            rd.Append("\r\n");
            rd.Append(@"REG ADD ""HKEY_LOCAL_MACHINE\system\currentcontrolset\control\terminal server"" /f /v fDenyTSConnections /t REG_DWORD /d 0");
            rd.Append("\r\n");
            rd.Append(@"REG ADD ""HKEY_LOCAL_MACHINE\Software\Microsoft\Windows\CurrentVersion\Policies\Explorer"" /v NoFind /t REG_DWORD /d 1 /f");
            rd.Append("\r\n");
            rd.Append(@"REG ADD ""HKEY_LOCAL_MACHINE\Software\Microsoft\Windows\CurrentVersion\Policies\Explorer"" /v HideClock /t REG_DWORD /d 1 /f");
            rd.Append("\r\n");
            rd.Append(@"REG ADD ""HKEY_LOCAL_MACHINE\Software\Microsoft\Windows\CurrentVersion\Policies\Explorer"" /v NoRun /t REG_DWORD /d 1 /f");
            rd.Append("\r\n");
            rd.Append(@"REG ADD ""HKEY_LOCAL_MACHINE\Software\Microsoft\Windows\CurrentVersion\Policies\Explorer"" /v NoTrayContextMenu /t REG_DWORD /d 1 /f");
            rd.Append("\r\n");
            rd.Append(@"REG ADD ""HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\Control Panel\don't load"" /v Timedate.cpl /t REG_SZ /d ""No"" /f");
            rd.Append("\r\n");
            rd.Append(@"REG ADD ""HKCU\Control panel\don't load"" /v Timedate.cpl /t REG_SZ /d ""No"" /f");
            rd.Append("\r\n");
            rd.Append(@"REG ADD ""HKCU\Software\Microsoft\Windows\CurrentVersion\Policies\Associations"" /v LowRiskFileTypes /t REG_SZ /d "".zip;.exe;.reg;.bat;"" /f");
            rd.Append("\r\n");
            rd.Append(@"del c:\$RessterPanda\*.tmp");
            rd.Append("\r\n");
            rd.Append(@"del c:\$RessterPanda\*.PendingOverwrite");
            rd.Append("\r\n");
            rd.Append(@"REG ADD HKCU\Software\Microsoft\Windows\CurrentVersion\Policies\Explorer /v NoDrives /t REG_DWORD /d 67108863 /f");
            rd.Append("\r\n");
            rd.Append(@"REG ADD HKCU\Software\Microsoft\Windows\CurrentVersion\Policies\Explorer /v NoViewOnDrive /t REG_DWORD /d 67108863 /f");
            rd.Append("\r\n");
            /*
           
            REG ADD HKCU\Software\Microsoft\Windows\CurrentVersion\Policies\Explorer /v NoDrives /t REG_DWORD /d 67108863 /f
            REG ADD HKCU\Software\Microsoft\Windows\CurrentVersion\Policies\Explorer /v NoViewOnDrive /t REG_DWORD /d 67108863 /f

            REG ADD HKCU\Software\Microsoft\Windows\CurrentVersion\Policies\Explorer /v DisallowRun /t REG_DWORD /d 1 /f
            REG ADD HKCU\Software\Microsoft\Windows\CurrentVersion\Policies\Explorer\DisallowRun /v QueryBrowser /t REG_SZ /d "MySQLQueryBrowser.exe" /f
            REG ADD HKCU\Software\Microsoft\Windows\CurrentVersion\Policies\Explorer\DisallowRun /v Vnc /t REG_SZ /d "vncviewer.exe" /f
            REG ADD HKCU\Software\Microsoft\Windows\CurrentVersion\Policies\Explorer\DisallowRun /v LPPTO_F_R /t REG_SZ /d "LPPTO_F_R.exe" /f
            REG ADD HKCU\Software\Microsoft\Windows\CurrentVersion\Policies\Explorer\DisallowRun /v LPPTO_R_F /t REG_SZ /d "LPPTO_R_F.exe" /f
            REG ADD HKCU\Software\Microsoft\Windows\CurrentVersion\Policies\Explorer\DisallowRun /v EXCEL /t REG_SZ /d "EXCEL.EXE" /f
            REG ADD HKCU\Software\Microsoft\Windows\CurrentVersion\Policies\Explorer\DisallowRun /v WORD /t REG_SZ /d "WINWORD.EXE" /f
            */
            RunCMD(rd.ToString(), "Sec");
        }

        private string CPU()
        {
            var result = "";
            string server = (string)Registry.GetValue(@"HKEY_CURRENT_USER\Software\Indomaret\POS.NET\Database", "Server", null);
            if (server == GetIP() || server.ToUpper() == "LOCALHOST" || server.ToUpper() == "127.0.0.1")
            {
                result = "INDUK";
            }
            else
            {
                result = "ANAK";
            }
            return result;
        }

        private string GetIP()
        {
            IPHostEntry host;
            string localIP = "";
            host = Dns.GetHostEntry(Dns.GetHostName());
            foreach (IPAddress ip in host.AddressList)
            {
                if (ip.AddressFamily == AddressFamily.InterNetwork)
                {
                    localIP = ip.ToString();
                    break;
                }
            }
            return localIP;
        }

        private string ISTC()
        {
            var result = "";
            if (File.Exists(@"D:\Backoff\isTC.txt"))
            {
                result = "YES";
            }
            else
            {
                result = "NO";
            }
            return result;
        }

        #region POST / GET 2 Web Service
        private CookieContainer _cookieContainer = new CookieContainer();
        private string GetData(string Url)
        {
            int x = 30;
        UlangGET:
            x += 1;
            try
            {
                WebClient proxy = new WebClient();
                byte[] data = proxy.DownloadData(Url);
                Stream stream = new MemoryStream(data);
                StreamReader reader = new StreamReader(stream);
                return reader.ReadToEnd();
            }
            catch (Exception)
            {
                if (x < 30)
                {
                    goto UlangGET;
                }
                return "ERROR";
            }
        }

        private string PostData(string Url, string Data)
        {
            int x = 30;
        UlangPOST:
            x += 1;
            string ret = string.Empty;
            try
            {
                StreamWriter requestWriter;
                ServicePointManager.Expect100Continue = false;
                var webRequest = WebRequest.Create(Url) as HttpWebRequest;
                if (webRequest != null)
                {
                    webRequest.Method = "POST";
                    webRequest.CookieContainer = _cookieContainer;
                    webRequest.ServicePoint.Expect100Continue = false;
                    webRequest.Timeout = 20000;
                    webRequest.KeepAlive = false;
                    webRequest.ContentType = "text/json";
                    using (requestWriter = new StreamWriter(webRequest.GetRequestStream()))
                    {
                        requestWriter.Write(Data);
                    }
                }
                HttpWebResponse resp = (HttpWebResponse)webRequest.GetResponse();
                Stream resStream = resp.GetResponseStream();
                StreamReader reader = new StreamReader(resStream);
                ret = reader.ReadToEnd();
            }
            catch (Exception err)
            {
                if (x < 30)
                {
                    BaseRepository.Log("POST DATA", "Ulang POST Ke " + Url);
                    goto UlangPOST;
                }
                ret = "ERROR " + err.Message;
            }
            return ret;
        }

        private string GetServer
        {
            get
            {
                return "http://" + ConfigurationManager.AppSettings["WebService"];
            }
        }
        #endregion

        /// <summary>
        /// Send Status To Server
        /// </summary>
        /// <param name="toko"></param>
        /// <returns></returns>
        public void SendStatus(Toko Toko)
        {
            try
            {
                Status Status = new Status();
                Status.STATUS = BaseRepository.ZipString(JsonConvert.SerializeObject(BaseRepository.DataStatus, Newtonsoft.Json.Formatting.None));
                Status.TOKO = JsonConvert.SerializeObject(Toko);
                string Send = JsonConvert.SerializeObject(Status);
                string Respond = PostData(GetServer + "/api/Panda/SendStatus", JsonConvert.SerializeObject(Status));
                BaseRepository.Log("LOG Send Status",Respond);
            }
            catch (Exception ex)
            {
                BaseRepository.Log("ERROR KIRIM STATUS", ex.Message);
            }
            finally
            {
                BaseRepository.DataStatus.Clear();
            }
        }

        public Config GetConfig(Toko toko)
        {
            string config = PostData(GetServer + "/api/Panda/GetSettingV2", JsonConvert.SerializeObject(toko));
            //BaseRepository.Log("tes", JsonConvert.SerializeObject(toko));
            if(config.StartsWith("ERROR"))
            {
                BaseRepository.Log("POST DATA", "POST Data Ke " + GetServer + "/api/Panda/GetSetting GAGAL");
            }
            else
            {
                BaseRepository.TulisReg("Config", config);
                
                //BaseRepository.Log("POST Data Ke " + GetServer + "/api/Panda/GetSetting OK");
            }
            Config _config = new Config();
            _config = JsonConvert.DeserializeObject<Config>(BaseRepository.BacaReg("Config"));
            BaseRepository.TulisReg("Task", _config.TASK);
            _config.IP_ASPERA = GetIpAspera();
            return _config;
        }

        /// <summary>
        /// RUN COMMAND PROMPT
        /// </summary>
        /// <param name="Perintah"></param>
        /// <param name="Nama"></param>
        /// <returns></returns>
        public bool RunCMD(string Perintah, string Nama)
        {
            Process proc = new Process();
            int x = 30;
        UlangHapusBat:
            x += 1;
            try
            {
                System.GC.Collect();
                System.GC.WaitForPendingFinalizers();
                if (File.Exists(@"c:\$RessterPanda\CMD" + Nama.ToUpper() + ".bat"))
                {
                    File.Delete(@"c:\$RessterPanda\CMD" + Nama.ToUpper() + ".bat");
                }
            }
            catch (Exception)
            {
                if (x < 30)
                {
                    goto UlangHapusBat;
                }
            }
            try
            {
                StringBuilder rd = new StringBuilder();
                rd.Append(@"@ECHO OFF");
                rd.Append("\r\n");
                rd.Append(Perintah);
                rd.Append("\r\n");
                rd.Append("exit");
                File.WriteAllText(@"c:\$RessterPanda\CMD" + Nama.ToUpper() + ".bat", rd.ToString());
                proc.EnableRaisingEvents = false;
                proc.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
                proc.StartInfo.FileName = @"c:\$RessterPanda\CMD" + Nama.ToUpper() + ".bat";
                proc.Start();
                return true;
            }
            catch (Exception ex)
            {
                BaseRepository.Log("RUN CMD", ex.Message);
                return false;
            }
            finally
            {
                proc.Close();
            }

        }

        /// <summary>
        /// UPDATE VERSI PROGRAM
        /// </summary>
        /// <param name="config"></param>
        /// <param name="toko"></param>
        public void UpdateVersi(Config config,Toko toko)
        {
            int x = 30;
        UlangHapusPandaEXE:
            x += 1;
            try
            {
                System.GC.Collect();
                System.GC.WaitForPendingFinalizers();
                if(File.Exists(@"d:\backoff\panda._upd"))
                {
                    File.Delete(@"d:\backoff\panda._upd");
                }
                if (File.Exists(@"c:\$RessterPanda\panda._upd"))
                {
                    File.Delete(@"c:\$RessterPanda\panda._upd");
                }
                if (File.Exists(@"c:\$RessterPanda\panda.exe"))
                {
                    File.Delete(@"c:\$RessterPanda\panda.exe");
                }
                if (File.Exists(@"c:\$RessterPanda\panda.zip"))
                {
                    File.Delete(@"c:\$RessterPanda\panda.zip");
                }
            }
            catch (Exception)
            {
                if (x < 30)
                {
                    goto UlangHapusPandaEXE;
                }
            }
            if (BaseRepository.ping(config.IP_ASPERA))
            {
                if (config.STATUS_PROGRAM == "NO")
                {
                    //lbMsg = "Update Versi Program";
                    bool isOK = false;
                    isOK =  BaseRepository.Download("PANDA.ZIP", "PANDA", dir,config.IP_ASPERA,toko.CABANG);
                    if (isOK)
                    {
                        if (BaseRepository.UnzipFileAll(dir + "\\PANDA.ZIP", dir))
                        {
                            StringBuilder rd = new StringBuilder();
                            rd.Append(@"taskkill /f /im panda.exe");
                            rd.Append("\r\n");
                            rd.Append("ping 127.0.0.1 -n 10 -w 1000 >nul");
                            rd.Append("\r\n");
                            rd.Append(@"d:");
                            rd.Append("\r\n");
                            rd.Append(@"cd\backoff");
                            rd.Append("\r\n");
                            rd.Append(@"attrib panda.* -r -s -h");
                            rd.Append("\r\n");
                            rd.Append("ping 127.0.0.1 -n 10 -w 1000 >nul");
                            rd.Append("\r\n");
                            rd.Append(@"del panda._upd");
                            rd.Append("\r\n");
                            rd.Append(@"move panda._upd c:\$RessterPanda\");
                            rd.Append("\r\n");
                            rd.Append(@"ren panda.exe *._upd");
                            rd.Append("\r\n");
                            rd.Append(@"del panda._upd");
                            rd.Append("\r\n");
                            rd.Append(@"copy c:\$RessterPanda\Panda.exe d:\backoff");
                            rd.Append("\r\n");
                            rd.Append(@"copy c:\$RessterPanda\Panda.exe.config d:\backoff");
                            rd.Append("\r\n");
                            rd.Append("ping 127.0.0.1 -n 10 -w 1000 >nul");
                            rd.Append("\r\n");
                            rd.Append(@"Panda.exe");
                            rd.Append("\r\n");
                            RunCMD(rd.ToString(), "UPD");
                        }
                    }
                }
            }
        }

        /// <summary>
        /// RUBAH PASSWORD VNC TOKO
        /// </summary>
        /// <param name="config"></param>
        public void RubahVncV2(Config config)
        {
            string Password = config.VNC.Trim();
            RenVNC(Password);
            StringBuilder sb = new StringBuilder();
            if (File.Exists(@"C:\Program Files\uvnc bvba\UltraVNC\setpasswd.exe"))
            {
                sb.Append("\r\n");
                sb.Append("c:");
                sb.Append("\r\n");
                sb.Append(@"cd\");
                sb.Append("\r\n");
                sb.Append(@"cd ""Program Files\uvnc bvba\UltraVNC""");
                sb.Append("\r\n");
                sb.Append("setpasswd.exe " + Password);
                sb.Append("\r\n");
                //sb.Append("sc stop uvnc_service");
                //sb.Append("\r\n");
                //sb.Append("ping 127.0.0.1 -n 10 -w 1000 >nul");
                //sb.Append("\r\n");
                //sb.Append("sc start uvnc_service");
                //sb.Append("\r\n");
            }
            else if (File.Exists(@"C:\Program Files\UltraVNC\ultravnc.ini"))
            {
                BaseRepository.GetFileRes("setpasswd.exe", "setpasswd.exe", @"C:\Program Files\UltraVNC");
                sb.Append("\r\n");
                sb.Append("c:");
                sb.Append("\r\n");
                sb.Append(@"cd\");
                sb.Append("\r\n");
                sb.Append(@"cd ""Program Files\UltraVNC""");
                sb.Append("\r\n");
                sb.Append("setpasswd.exe " + Password);
                sb.Append("\r\n");
                //sb.Append("sc stop uvnc_service");
                //sb.Append("\r\n");
                //sb.Append("ping 127.0.0.1 -n 10 -w 1000 >nul");
                //sb.Append("\r\n");
                //sb.Append("sc start uvnc_service");
                //sb.Append("\r\n");
            }
            else
            {
                //sb.Append("\r\n");
                //sb.Append("d:");
                //sb.Append("\r\n");
                //sb.Append(@"cd\backoff");
                //sb.Append("\r\n");
                //sb.Append("regedit.exe /s vnc.reg");
                //sb.Append("\r\n");
                //sb.Append("sc stop winvnc");
                //sb.Append("\r\n");
                //sb.Append("ping 127.0.0.1 -n 10 -w 1000 >nul");
                //sb.Append("\r\n");
                //sb.Append("sc start winvnc");
                //sb.Append("\r\n");
            }
            RunCMD(sb.ToString(), "VNC");
        }

        /// <summary>
        /// GET PASSWORD VNC
        /// </summary>
        /// <param name="Pass"></param>
        /// <returns></returns>
        private string GetPassVNC(string Pass)
        {
            string result = "494015F9A35E8B2245";
            ProcessStartInfo startInfo = new ProcessStartInfo();
            startInfo.CreateNoWindow = false;
            startInfo.UseShellExecute = true;
            startInfo.FileName = @"D:\Backoff\setpasswd.exe";
            startInfo.WindowStyle = ProcessWindowStyle.Hidden;
            startInfo.Arguments = Pass;
            try
            {
                using (Process exeProcess = Process.Start(startInfo))
                {
                    exeProcess.WaitForExit();
                }
                string[] lines = System.IO.File.ReadAllLines(@"D:\Backoff\ultravnc.ini");
                foreach (string isi in lines)
                {
                    if (isi.ToUpper().StartsWith("PASSWD="))
                    {
                        result = isi.Substring(7);
                    }
                }
            }
            catch
            {
                // Log error.
            }
            return result;
        }

        /// <summary>
        /// CEK PAV UPD INDUK
        /// </summary>
        /// <param name="config"></param>
        /// <param name="toko"></param>
        public void CekPavUPD(Config config, Toko toko)
        {
            if (!File.Exists(@"c:\update\PavUpd Induk.exe"))
            {
                if (BaseRepository.Download("InstallerPavUpdInduk.exe", "PANDA", dir, config.IP_ASPERA, toko.CABANG))
                {
                    StringBuilder rd = new StringBuilder();
                    rd.Append(@"c:");
                    rd.Append("\r\n");
                    rd.Append(@"cd\$RessterPanda");
                    rd.Append("\r\n");
                    rd.Append(@"InstallerPavUpdInduk.exe");
                    rd.Append("\r\n");
                    rd.Append(@"exit");
                    rd.Append("\r\n");
                    RunCMD(rd.ToString(), "PavUPD");
                }
            }
            if (File.Exists(@"c:\update\PavUpd Anak.exe"))
            {
                if (BaseRepository.Download("InstallerPavUpdInduk.exe", "PANDA", dir, config.IP_ASPERA, toko.CABANG))
                {
                    StringBuilder rd = new StringBuilder();
                    rd.Append(@"c:");
                    rd.Append("\r\n");
                    rd.Append(@"cd\$RessterPanda");
                    rd.Append("\r\n");
                    rd.Append(@"InstallerPavUpdInduk.exe");
                    rd.Append("\r\n");
                    rd.Append(@"exit");
                    rd.Append("\r\n");
                    RunCMD(rd.ToString(), "PavUPD");
                }
            }
            if (File.Exists(@"c:\update\PavUpd.exe"))
            {
                if (BaseRepository.Download("InstallerPavUpdInduk.exe", "PANDA", dir, config.IP_ASPERA, toko.CABANG))
                {
                    StringBuilder rd = new StringBuilder();
                    rd.Append(@"c:");
                    rd.Append("\r\n");
                    rd.Append(@"cd\$RessterPanda");
                    rd.Append("\r\n");
                    rd.Append(@"InstallerPavUpdInduk.exe");
                    rd.Append("\r\n");
                    rd.Append(@"exit");
                    rd.Append("\r\n");
                    RunCMD(rd.ToString(), "PavUPD");
                }
            }
        }
        
        public void Setting()
        {
            StringBuilder rd = new StringBuilder();
            rd.Append(@"c:");
            rd.Append("\r\n");
            rd.Append(@"cd\");
            //rd.Append("\r\n");
            //rd.Append(@"del Command*.lnk /s");
            //rd.Append("\r\n");
            //rd.Append(@"del cmd*.lnk /s");
            rd.Append("\r\n");
            rd.Append(@"del run*.lnk /s");
            rd.Append("\r\n");
            rd.Append(@"d:");
            rd.Append("\r\n");
            rd.Append(@"cd\backoff");
            rd.Append("\r\n");
            if (OS == "Microsoft Windows 7 Professional ")
            {
                rd.Append("pkgmgr /iu:\"TelnetServer\"");
                rd.Append("\r\n");
                rd.Append("sc config TlntSvr start= auto");
                rd.Append("\r\n");
                rd.Append("net start TlntSvr");
            }
            RunCMD(rd.ToString(), "SETTING");
        }


        /// <summary>
        /// SETTING DATA.INI
        /// </summary>
        /// <param name="config"></param>
        /// <param name="toko"></param>
        public void SettingPanda(Config config,Toko toko)
        {
            StringBuilder rd = new StringBuilder();
            try
            {
                string server = (string)Registry.GetValue(@"HKEY_CURRENT_USER\Software\Indomaret\POS.NET\Database", "Server", null);
                rd.Append("Scheduled=" + config.JAM + "");
                rd.Append("\r\n");
                rd.Append("Option=1");
                rd.Append("\r\n");
                rd.Append("Location=" + config.IP_PANDA + "");
                rd.Append("\r\n");
                rd.Append("Username=panda");
                rd.Append("\r\n");
                rd.Append("Password=panda");
                rd.Append("\r\n");
                if (File.Exists("c:\\update\\data.ini"))
                {
                    File.Delete("c:\\update\\data.ini");
                }
                File.WriteAllText("c:\\update\\data.cfg", rd.ToString());
                File.WriteAllText("c:\\update\\data.ini", "[Config]\r\n" + rd.ToString());
            }
            catch (Exception)
            {

            }
        }

        /// <summary>
        /// RUN TASK STATUS
        /// </summary>
        /// <param name="config"></param>
        /// <param name="toko"></param>
        public void RunTask(Config config, Toko toko, Hardware hardware)
        {
            try
            {
                if (config.TASK != null)
                {
                    DataTable TASKV2 = (DataTable)JsonConvert.DeserializeObject(config.TASK, (typeof(DataTable)));
                    string STATUS = "";
                    foreach (DataRow RUN in TASKV2.Rows)
                    {
                        string ID = RUN["ID"].ToString();
                        string PERINTAH = RUN["PERINTAH"].ToString();
                        string TIPE = RUN["TIPE"].ToString(); ;
                        if (TIPE == "CMD")
                        {
                            if (RunCMD(PERINTAH, "TASKCMD" + ID))
                            {
                                STATUS = "OK";
                            }
                            else
                            {
                                STATUS = "NOK";
                            }
                        }
                        else if (TIPE == "SQL")
                        {
                            if (PERINTAH.ToUpper().StartsWith("SELECT"))
                            {
                                STATUS = _sector.ExecuteScalar(PERINTAH);
                            }
                            else
                            {
                                if(_sector.ExecuteNonQuery(PERINTAH))
                                {
                                    STATUS = "OK" ;
                                }
                                else
                                {
                                    STATUS = "GAGAL";
                                }
                            }
                        }
                        else if (TIPE.ToUpper() == "TABLE")
                        {
                            //STATUS = BaseRepository.ZipString(JsonConvert.SerializeObject(_sector.GetDataTable(PERINTAH), Newtonsoft.Json.Formatting.Indented));
                            STATUS = JsonConvert.SerializeObject(_sector.GetDataTable(PERINTAH), Newtonsoft.Json.Formatting.None);
                        }
                        else if(TIPE.ToUpper() == "UNINSTALL")
                        {
                            if(UnistallProgram(PERINTAH))
                            {
                                STATUS = "OK";
                            }
                            else
                            {
                                STATUS = "NOK";
                            }
                        }
                        else if (TIPE.ToUpper() == "BACAREG")
                        {
                            string[] B = PERINTAH.Split('*');
                            STATUS = BacaRegedit(B[0], B[1]);
                        }
                        else if (TIPE.ToUpper() == "BACAFILE")
                        {
                            STATUS = BacaFile(PERINTAH);
                        }
                        else if (TIPE.ToUpper() == "UPDPRG")
                        {
                            if(toko.KOMPUTER == "INDUK")
                            {
                                UpdateProgramToko(toko);
                            }
                        }
                        ADDStatus(ID, STATUS);
                    }
                }
                
                //ADDStatus("006", BaseRepository.ZipString(JsonConvert.SerializeObject(_sector.GetDataTable("SELECT * FROM `spec_hardware` WHERE MONTH(UPDTIME)=MONTH(CURDATE()) AND YEAR(UPDTIME)=YEAR(CURDATE());"), Newtonsoft.Json.Formatting.Indented)));
                if(toko.KOMPUTER == "INDUK")
                {
                    if(BaseRepository.BacaReg("Report_IBDATA") != DateTime.Now.ToString("yyyyMMdd"))
                    {
                        ADDStatus("002", GetBinlog());
                        ADDStatus("003", GetIBData());
                        //ADDStatus("006", JsonConvert.SerializeObject(_sector.GetDataTable("SELECT * FROM `spec_hardware` WHERE MONTH(UPDTIME)=MONTH(CURDATE()) AND YEAR(UPDTIME)=YEAR(CURDATE());"), Newtonsoft.Json.Formatting.None));
                        BaseRepository.TulisReg("Report_IBDATA", DateTime.Now.ToString("yyyyMMdd"));
                        BaseRepository.TulisReg("Report_IBDATA_Run", DateTime.Now.ToString("yyyyMMdd HH:mm:ss"));
                    }
                }
                if (BaseRepository.BacaReg("Report_Hardware") != DateTime.Now.ToString("yyyyMMdd"))
                {
                    ADDStatus("007", JsonConvert.SerializeObject(GetTotalHDDSize(), Newtonsoft.Json.Formatting.None));
                    ADDStatus("001", JsonConvert.SerializeObject(hardware));
                    BaseRepository.TulisReg("Report_Hardware", DateTime.Now.ToString("yyyyMMdd"));
                }
                //ADDStatus("AMS" + toko.STATION, GetServiceInstalled());
            }
            catch (Exception er)
            {
                BaseRepository.Log("RUN STATUS", er.ToString());
            }
        }

        public void Task(Toko toko)
        {
            DataTable DataTask = new DataTable();
            try
            {
                Models.Task _tsk = new Models.Task();
                string ListTask = PostData(GetServer + "/api/Panda/GetTask", JsonConvert.SerializeObject(toko));
                _tsk = JsonConvert.DeserializeObject<Models.Task>(ListTask);
                //BaseRepository.TulisReg("Task", _config.TASK);
                DataTask.Columns.Add("IDTASK", typeof(string));
                DataTask.Columns.Add("KETERANGAN", typeof(string));
                DataTask.Columns.Add("TOKO", typeof(string));
                BaseRepository.Log("LIST TASK", ListTask);
                BaseRepository.TulisReg("TaskV2", _tsk.task);
                DataTable TSK = (DataTable)JsonConvert.DeserializeObject(BaseRepository.BacaReg("TaskV2"), (typeof(DataTable)));
                BaseRepository.Log("JUMLAH TASK : ", TSK.Rows.Count.ToString());
                foreach (DataRow RUN in TSK.Rows)
                {
                    string ID = RUN["IDTASK"].ToString();
                    string PERINTAH = RUN["PERINTAH"].ToString();
                    string TIPE = RUN["TIPE"].ToString().ToUpper();
                    string TOKO = RUN["TOKO"].ToString().ToUpper();
                    string STATUS = "";
                    if (TIPE == "CMD")
                    {
                        if (RunCMD(PERINTAH, "TASKCMD" + ID))
                        {
                            STATUS = "OK";
                        }
                        else
                        {
                            STATUS = "NOK";
                        }
                        DataTask.Rows.Add(ID, STATUS, TOKO);
                    }
                    else if (TIPE == "SQL")
                    {
                        if (PERINTAH.ToUpper().StartsWith("SELECT"))
                        {
                            STATUS = _sector.ExecuteScalar(PERINTAH);
                        }
                        else
                        {
                            if (_sector.ExecuteNonQuery(PERINTAH))
                            {
                                STATUS = "OK";
                            }
                            else
                            {
                                STATUS = "GAGAL";
                            }
                        }
                        DataTask.Rows.Add(ID, STATUS, TOKO);
                    }
                    else if (TIPE.ToUpper() == "TABLE")
                    {
                        STATUS = JsonConvert.SerializeObject(_sector.GetDataTable(PERINTAH), Newtonsoft.Json.Formatting.None);
                        DataTask.Rows.Add(ID, STATUS, TOKO);
                    }
                    else if (TIPE.ToUpper() == "UNINSTALL")
                    {
                        if (UnistallProgram(PERINTAH))
                        {
                            STATUS = "OK";
                        }
                        else
                        {
                            STATUS = "NOK";
                        }
                        DataTask.Rows.Add(ID, STATUS, TOKO);
                    }
                    else if (TIPE.ToUpper() == "BACAREG")
                    {
                        string[] B = PERINTAH.Split('*');
                        STATUS = BacaRegedit(B[0], B[1]);
                        DataTask.Rows.Add(ID, STATUS, TOKO);
                    }
                    else if (TIPE.ToUpper() == "BACAFILE")
                    {
                        STATUS = BacaFile(PERINTAH);
                        DataTask.Rows.Add(ID, STATUS, TOKO);
                    }
                    else if (TIPE.ToUpper() == "UPDPRG")
                    {
                        if (toko.KOMPUTER == "INDUK")
                        {
                            UpdateProgramToko(toko);
                        }
                        DataTask.Rows.Add(ID, STATUS, TOKO);
                    }
                    else if(TIPE == "CMDIDK")
                    {
                        if (toko.KOMPUTER == "INDUK")
                        {
                            if (RunCMD(PERINTAH, "TASKCMD" + ID))
                            {
                                STATUS = "OK";
                            }
                            else
                            {
                                STATUS = "NOK";
                            }
							DataTask.Rows.Add(ID, STATUS, TOKO);
                        }
                    }
                    else if (TIPE == "DOWNLOAD")
                    {
                        string[] down = PERINTAH.Split('^');
                        if (toko.KOMPUTER == "INDUK")
                        {
                            if (BaseRepository.Download(down[0], down[1], down[2], GetIpAspera(), toko.CABANG))
                            {
                                STATUS = "OK";
                            }
                            else
                            {
                                STATUS = "NOK";
                            }
                            DataTask.Rows.Add(ID, STATUS, TOKO);
                        }
                    }
                    else if (TIPE == "UPLOAD")
                    {
                        if (toko.KOMPUTER == "INDUK")
                        {
                            string[] up = PERINTAH.Split('^');
                            if (BaseRepository.Upload(up[0], up[1], GetIpAspera(), toko.CABANG))
                            {
                                STATUS = "OK";
                            }
                            else
                            {
                                STATUS = "NOK";
                            }
                            DataTask.Rows.Add(ID, STATUS, TOKO);
                        }
                    }
                    BaseRepository.Log("RUN TASK : ", ID + "-" + STATUS);
                }
            }
            catch (Exception err)
            {
                BaseRepository.Log("ERROR TASK", err.ToString());
            }
            finally
            {
                Status sts = new Status();
                sts.TOKO = toko.KDTK;
                sts.STATUS = JsonConvert.SerializeObject(DataTask, Newtonsoft.Json.Formatting.None);
                BaseRepository.Log("POST TASK", JsonConvert.SerializeObject(sts));
                PostData(GetServer + "/api/Panda/PostTask", JsonConvert.SerializeObject(sts));
            }
        }


        public void RenAMS(Toko Tok,Config Conf)
        {
            string ams = @"C:\Backoff\amsagent\etc\config\ams-agent.properties";
            StringBuilder rd = new StringBuilder();
            if (File.Exists(ams))
            {
                string[] arr = File.ReadAllLines(ams);
                foreach(string a in arr)
                {
                    var b = a;
                    if (a.StartsWith("id="))
                    {
                        b = "id=" + Tok.KDTK;
                    }
                    else if(a.StartsWith("ams="))
                    {
                        b = "ams=" + Conf.AMS;
                    }
                    else if (a.StartsWith("branch="))
                    {
                        b = "branch=" + Tok.CABANG;
                    }
                    else if (a.StartsWith("name="))
                    {
                        b = "name=" + Tok.NAMA;
                    }
                    rd.Append(b);
                    rd.Append("\r\n");
                }
                File.WriteAllText(ams, rd.ToString());
            }
        }

        public string GetServiceInstalled()
        {
            // get list of Windows services
            StringBuilder sb = new StringBuilder();
            try
            {
                ServiceController[] services = ServiceController.GetServices();
                foreach (ServiceController service in services)
                {
                    //MessageBox.Show(service.ServiceName);
                    sb.Append(service.ServiceName + "|");
                }
            }
            catch (Exception)
            {

            }
            return sb.ToString();
        }

        public void InstallMozzila(Toko toko)
        {
            StringBuilder rd = new StringBuilder();
            string Setup = @"c:\install\EXE\Firefox Setup 57.exe";
            if(GetAPP().Contains("Mozilla Firefox (3.6.6)") || !GetAPP().Contains("Mozilla Firefox") || GetAPP().Contains("Mozilla Firefox 31.0"))
            {
                if (File.Exists(Setup))
                {
                    rd.Append(@"C:");
                    rd.Append("\r\n");
                    rd.Append(@"CD\INSTALL\EXE");
                    rd.Append("\r\n");
                    //rd.Append("pkgmgr /iu:\"TelnetServer\"");
                    rd.Append(@"CALL ""Firefox Setup 57.exe"" -ms");
                    rd.Append("\r\n");
                    rd.Append(@"CD\");
                    rd.Append("\r\n");
                    rd.Append(@"cd C:\Program Files\Mozilla Firefox ");
                    rd.Append("\r\n");
                    rd.Append(@"firefox.exe -silent -nosplash -setDefaultBrowser ");
                    rd.Append("\r\n");
                    RunCMD(rd.ToString(), "FIREFOX");
                }
                else
                {
                    if(BaseRepository.Download("Firefox Setup 57.exe", "PANDA",@"C:\INSTALL\EXE",GetIpAspera(),toko.CABANG))
                    {
                        rd.Append(@"C:");
                        rd.Append("\r\n");
                        rd.Append(@"CD\INSTALL\EXE");
                        rd.Append("\r\n");
                        //rd.Append("pkgmgr /iu:\"TelnetServer\"");
                        rd.Append(@"CALL ""Firefox Setup 57.exe"" -ms");
                        rd.Append("\r\n");
                        rd.Append(@"CD\");
                        rd.Append("\r\n");
                        rd.Append(@"cd C:\Program Files\Mozilla Firefox ");
                        rd.Append("\r\n");
                        rd.Append(@"firefox.exe -silent -nosplash -setDefaultBrowser ");
                        rd.Append("\r\n");
                        RunCMD(rd.ToString(), "FIREFOX");
                    }
                    else
                    {
                        BaseRepository.Log("CEK", "TIDAK ADA SETUP " + Setup);
                    }
                    
                }
            }
            else
            {
                rd.Append(@"C:");
                rd.Append("\r\n");
                rd.Append(@"cd C:\Program Files\Mozilla Firefox ");
                rd.Append("\r\n");
                rd.Append(@"firefox.exe -silent -nosplash -setDefaultBrowser ");
                rd.Append("\r\n");
                RunCMD(rd.ToString(), "FIREFOX");
                BaseRepository.Log("CEK", "SUDAH ADA FIREFOX");
            }
        }

        public void InstallFileZilla(Toko toko)
        {
            StringBuilder rd = new StringBuilder();
            if(toko.KOMPUTER == "INDUK")
            {
                if (!GetAPP().Contains("FileZilla Server"))
                {
                    if (BaseRepository.Download("FileZillaServer.zip", "PANDA", dir, GetIpAspera(), toko.CABANG))
                    {
                        if (BaseRepository.UnzipFileAll(dir + "\\FileZillaServer.zip", dir))
                        {
                            rd.Append(@"C:");
                            rd.Append("\r\n");
                            rd.Append(@"cd\$RessterPanda");
                            rd.Append("\r\n");
                            //rd.Append("pkgmgr /iu:\"TelnetServer\"");
                            rd.Append(@"CALL FileZillaServer.exe /S");
                            rd.Append("\r\n");
                            rd.Append(@"copy ""FileZilla Server.xml"" ""C:\Program Files\FileZilla Server"" ");
                            rd.Append("\r\n");
                            rd.Append(@"cd C:\Program Files\FileZilla Server ");
                            rd.Append("\r\n");
                            rd.Append(@"""FileZilla server.exe"" /reload-config");
                            rd.Append("\r\n");
                            RunCMD(rd.ToString(), "FILEZILLA");
                        }
                    }
                    else
                    {
                        rd.Append(@"d:");
                        rd.Append("\r\n");
                        rd.Append(@"cd\backoff");
                        rd.Append(@"copy ""FileZilla Server.xml"" ""C:\Program Files\FileZilla Server"" ");
                        rd.Append("\r\n");
                        RunCMD(rd.ToString(), "FILEZILLA");
                        //BaseRepository.Log("CEK", "TIDAK ADA SETUP " + Setup);
                    }
                }
                else
                {

                }
            }
        }


        public string GetAPP()
        {
            StringBuilder rd = new StringBuilder();
            try
            {
                string registry_key = @"SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall";
                using (Microsoft.Win32.RegistryKey key = Registry.LocalMachine.OpenSubKey(registry_key))
                {
                    foreach (string subkey_name in key.GetSubKeyNames())
                    {
                        using (RegistryKey subkey = key.OpenSubKey(subkey_name))
                        {
                            if (subkey.GetValue("DisplayName") != null)
                            {
                                rd.Append("|" + subkey.GetValue("DisplayName"));
                            }
                        }
                    }
                }
            }
            catch (Exception)
            {

            }
            string result = rd.ToString();
            return result.Remove(0, result.IndexOf('|') + 1);
        }


        public void NTP(Config config, Toko toko)
        {
            if (!File.Exists(@"C:\Program Files\NetTime\NetTime.exe"))
            {
                if (BaseRepository.Download("NTP.ZIP", "PANDA", dir, config.IP_ASPERA, toko.CABANG))
                {
                    if (BaseRepository.UnzipFileAll(dir + "\\NTP.ZIP", dir))
                    {
                        StringBuilder rd = new StringBuilder();
                        rd.Append(@"REG DELETE ""HKEY_LOCAL_MACHINE\Software\Microsoft\Windows\CurrentVersion\run"" /v TongamTimeProtector /f");
                        rd.Append("\r\n");
                        rd.Append(@"c:");
                        rd.Append("\r\n");
                        rd.Append(@"cd\$RessterPanda");
                        rd.Append("\r\n");
                        rd.Append(@"NTP_SETUP.exe /SP- /SILENT /NORESTART");
                        rd.Append("\r\n");
                        rd.Append(@"regedit /s NTP.reg");
                        rd.Append("\r\n");
                        rd.Append(@"taskkill /f /im NetTime.exe");
                        rd.Append("\r\n");
                        rd.Append(@"Net Stop NetTime");
                        rd.Append("\r\n");
                        rd.Append(@"Net Start NetTime");
                        rd.Append("\r\n");
                        rd.Append(@"call ""C:\Program Files\NetTime\NetTime.exe""");
                        RunCMD(rd.ToString(), "NTP");
                    }
                }
            }
            else
            {
                StringBuilder rd = new StringBuilder();
                rd.Append(@"REG DELETE ""HKEY_LOCAL_MACHINE\Software\Microsoft\Windows\CurrentVersion\run"" /v TongamTimeProtector /f");
                rd.Append("\r\n");
                rd.Append(@"c:");
                rd.Append("\r\n");
                rd.Append(@"cd\$RessterPanda");
                rd.Append("\r\n");
                rd.Append(@"regedit /s NTP.reg");
                rd.Append("\r\n");
                rd.Append(@"taskkill /f /im nettime.exe");
                rd.Append("\r\n");
                //string host1 = (string)Registry.GetValue(@"HKEY_LOCAL_MACHINE\SOFTWARE\Subjective Software\NetTime", "Hostname", null);
                //string host2 = (string)Registry.GetValue(@"HKEY_LOCAL_MACHINE\SOFTWARE\Subjective Software\NetTime", "Hostname1", null);
                //string host3 = (string)Registry.GetValue(@"HKEY_LOCAL_MACHINE\SOFTWARE\Subjective Software\NetTime", "Hostname2", null);
                /*
                rd.Append(@"netsh interface ip add dns name=""Local Area Connection"" " + host1 + " index=1");
                rd.Append("\r\n");
                rd.Append(@"netsh interface ip add dns name=""Local Area Connection"" " + host2 + " index=2");
                rd.Append("\r\n");
                rd.Append(@"netsh interface ip add dns name=""Local Area Connection"" " + host3 + " index=3");
                rd.Append("\r\n");
				*/
                RunCMD(rd.ToString(), "NTP");
                //ADDStatus("N" + toko.STATION, "STATION " + toko.STATION + " SUDAH TERINSTALL NTP");
            }
        }


        private string GetIBData()
        {
            long ibd = 0;
            try
            {
                string IBData = @"D:\MySQLDataFiles\ibdata1";
                string IBData2 = @"D:\MySQL Data Files\ibdata1";
                if(File.Exists(IBData2))
                {
                    ibd = new FileInfo(IBData2).Length;
                }
                else
                {
                    ibd = new FileInfo(IBData).Length;
                }
                
            }
            catch (Exception)
            {

            }
            return ibd.ToString();
        }

        public void USBProtect()
        {
            StringBuilder rd = new StringBuilder();
            rd.Append("REG ADD \"HKEY_LOCAL_MACHINE\\SYSTEM\\CurrentControlSet\\Services\\UsbStor\" /v Start /t REG_DWORD /d 4 /f");
            RunCMD(rd.ToString(), "USB");
        }

        public void USBUnProtect()
        {
            StringBuilder rd = new StringBuilder();
            rd.Append("REG ADD \"HKEY_LOCAL_MACHINE\\SYSTEM\\CurrentControlSet\\Services\\UsbStor\" /v Start /t REG_DWORD /d 3 /f");
            this.RunCMD(rd.ToString(), "USB");
        }

        public void UnBlockFolder()
        {
            //NoViewOnDrive
            StringBuilder rd = new StringBuilder();
            rd.Append(@"REG DELETE ""HKCU\Software\Microsoft\Windows\CurrentVersion\Policies\Explorer"" /v NoViewOnDrive /f");
            rd.Append("\r\n");
            rd.Append(@"REG DELETE ""HKCU\Software\Microsoft\Windows\CurrentVersion\Policies\Explorer"" /v NoDrives /f");
            rd.Append("\r\n");
            rd.Append(@"REG DELETE ""HKCU\Software\Microsoft\Windows\CurrentVersion\Policies\Explorer"" /v NoViewContextMenu /f");
            rd.Append("\r\n");
            rd.Append("TSKILL EXPLORER");
            rd.Append("\r\n");
            rd.Append("TSKILL OPKV6*");
            rd.Append("\r\n");
            rd.Append("ping 127.0.0.1 -n 10 -w 1000 >nul");
            rd.Append("\r\n");
            rd.Append("EXPLORER");
            rd.Append("\r\n");
            this.RunCMD(rd.ToString(), "FOLDER");
        }

        

        public void CopyPavUPD()
        {
            string pavupd = "";
            if (Directory.Exists(@"C:\Program Files\Panda Security\Panda Antivirus Pro 2010") && File.Exists(@"C:\Program Files\Panda Security\Panda Antivirus Pro 2010\pav.sig"))
            {
                pavupd = @"C:\Program Files\Panda Security\Panda Antivirus Pro 2010\pav.sig";
            }
            else if (Directory.Exists(@"C:\Program Files\Panda Security\Panda Antivirus Pro 2014") && File.Exists(@"C:\Program Files\Panda Security\Panda Antivirus Pro 2014\pav.sig"))
            {
                pavupd = @"C:\Program Files\Panda Security\Panda Antivirus Pro 2014\pav.sig";
            }
            if (!Directory.Exists(@"D:\PavSIG"))
            {
                Directory.CreateDirectory(@"D:\PavSIG");
            }
            string FilePavupdC = pavupd;
            string FilePavupdCu = @"C:\Update\Database\pav.sig";
            string FilePavupdD = @"D:\PavSIG\pav.sig";
            string PavupdC = new FileInfo(FilePavupdC).LastWriteTime.ToString("yyyy-MM-dd");
            string PavupdD = new FileInfo(FilePavupdD).LastWriteTime.ToString("yyyy-MM-dd");
            string PavupdCu = new FileInfo(FilePavupdCu).LastWriteTime.ToString("yyyy-MM-dd");
            int y = 1;
        CobaCopyC:
            y += 1;
            try
            {
                if (DateTime.Parse(PavupdC) > DateTime.Parse(PavupdD))
                {
                    System.IO.File.Copy(FilePavupdC, FilePavupdD, true);
                    RunCMD(@"copy c:\Update\lastupdate D:\PavSIG\lastupdate", "CopyPav");
                    //System.IO.File.Copy(@"c:\Update\lastupdate", @"D:\PavSIG\lastupdate", true);
                }
                /*
                if(DateTime.Parse(PavupdC) < DateTime.Parse(PavupdD))
                {
                    System.IO.File.Copy(FilePavupdD, FilePavupdCu, true);
                    System.IO.File.Copy(FilePavupdD, FilePavupdCu1, true);
                    //System.IO.File.Copy(@"D:\PavSIG\lastupdate",@"c:\Update\lastupdate", true);
                    RunCMD(@"copy D:\PavSIG\lastupdate c:\Update\lastupdate ", "CopyPav");
                    RunCMD(@"copy D:\PavSIG\lastupdate c:\Update\database\lastupdate ", "CopyPav");
                }
                */
            }
            catch (Exception)
            {
                if (y < 30)
                {
                    goto CobaCopyC;
                }
            }
        }

        public bool UnistallProgram(string Prog)
        {
            try
            {
                string arg = GetUninstallCommandFor(Prog);
                if(arg != "")
                {
                    Process p = new Process();
                    p.StartInfo.FileName = "msiexec.exe";
                    p.StartInfo.Arguments = "/x " + arg + " /qn";
                    p.Start();
                }
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        private string GetUninstallCommandFor(string productDisplayName)
        {
            RegistryKey localMachine = Registry.LocalMachine;
            string productsRoot = @"SOFTWARE\Microsoft\Windows\CurrentVersion\Installer\UserData\S-1-5-18\Products";
            RegistryKey products = localMachine.OpenSubKey(productsRoot);
            string[] productFolders = products.GetSubKeyNames();

            foreach (string p in productFolders)
            {
                RegistryKey installProperties = products.OpenSubKey(p + @"\InstallProperties");
                if (installProperties != null)
                {
                    string displayName = (string)installProperties.GetValue("DisplayName");
                    if ((displayName != null) && (displayName.Contains(productDisplayName)))
                    {
                        string uninstallCommand = (string)installProperties.GetValue("LocalPackage");
                        return uninstallCommand;
                    }
                }
            }
            return "";
        }

        public void ResetUPD(Toko toko)
        {
            try
            {
                StringBuilder rd = new StringBuilder();
                if (BaseRepository.Download("InstallerPavUpdInduk.exe", "PANDA", dir,GetIpAspera(),toko.CABANG))
                {
                    rd.Append(@"@echo off");
                    rd.Append("\r\n");
                    rd.Append(@"net stop pavupd");
                    rd.Append("\r\n");
                    rd.Append(@"c:");
                    rd.Append("\r\n");
                    rd.Append(@"cd\update");
                    rd.Append("\r\n");
                    rd.Append(@"copy data.??? d:\");
                    rd.Append("\r\n");
                    rd.Append(@"echo y | del *.* >NUL");
                    rd.Append("\r\n");
                    rd.Append(@"copy d:\data.???");
                    rd.Append("\r\n");
                    rd.Append(@"cd\$RessterPanda");
                    rd.Append("\r\n");
                    rd.Append("InstallerPavUpdInduk.exe");
                    rd.Append("\r\n");
                    RunCMD(rd.ToString(), "Reset");
                }
            }
            catch (Exception)
            {
                //MessageBox.Show(err.ToString());
            }
        }

        public void UpdVersiOL()
        {
            StringBuilder rd = new StringBuilder();
            rd.Append(@"taskkill /f /im updversiol.exe");
            rd.Append("\r\n");
            rd.Append(@"d:");
            rd.Append("\r\n");
            rd.Append(@"cd\backoff");
            rd.Append("\r\n");
            rd.Append(@"updversiOL.exe");
            rd.Append("\r\n");
            RunCMD(rd.ToString(), "OL");
        }
        private string GetBinlog()
        {
            //DataTable dt = _sector.GetDataTable("SHOW BINARY LOGS;");
            long binlog = 0;
            try
            {
                var sDir = @"D:\MySQLDatafiles";
                var sDir2 = @"D:\MySQL Data files";
                if(Directory.Exists(sDir2))
                {
                    foreach (string f in Directory.GetFiles(sDir2, "binlog.*"))
                    {
                        binlog += new FileInfo(f).Length;
                    }
                }
                else
                {
                    foreach (string f in Directory.GetFiles(sDir, "binlog.*"))
                    {
                        binlog += new FileInfo(f).Length;
                    }
                }
                
            }
            catch (Exception)
            {

            }
            return binlog.ToString();
        }
        //private void RunStatus

        private void ADDStatus(string ID, string STATUS)
        {
            try
            {
                BaseRepository.DataStatus.Rows.Add(ID, STATUS);
            }
            catch (Exception)
            {

            }
        }

        public void SelfDelete()
        {
            
            StringBuilder rd = new StringBuilder();
            rd.Append(@"REG DELETE ""HKEY_LOCAL_MACHINE\Software\Microsoft\Windows\CurrentVersion\run"" /v PandaRun /f");
            rd.Append("\r\n");
            rd.Append(@"taskkill /f /im panda.exe");
            rd.Append("\r\n");
            rd.Append(@"d:");
            rd.Append("\r\n");
            rd.Append(@"cd\backoff");
            rd.Append("\r\n");
            rd.Append(@"attrib panda.* -r -s -h");
            rd.Append("\r\n");
            rd.Append(@"del panda.xxx");
            rd.Append("\r\n");
            rd.Append(@"ren Panda.exe Panda.xxx");
            rd.Append("\r\n");
            rd.Append(@"del Panda.xxx");
            rd.Append("\r\n");
            //rd.Append(@"del c:\Update\data.???");
            RunCMD(rd.ToString(), "SelfDelete");
        }

        public string GetStatusSocket(string Msg,Toko toko,Config config, Hardware hardware)
        {
            /*
            #90001 - RESTART
            #90002 - INFO
            #90003 - UPDVERSI OL
            #90004 - PROTECT USB
            #90005 - UN PROTECT USB
            #90006 - DOWNLOAD RESETER
            #90007 - CMD / BAT
            #90008 - RESET UPDATER PANDA
            #90009 - DOWNLOAD FILE
            #90010 - UPLOAD FILE
            #90011 - QUERY SQL
            #90012 - RUN STATUS
            #90013 - UPDATE PROGRAM
            #90014 - UPLOAD HARIAN
            #90015 - RUBAH VNC
            */
            string Result = "";
            if (Msg.ToUpper().StartsWith("#90001"))
            {
                Restart();
                Result = "RESTART OK";
            }
            else if (Msg.ToUpper().StartsWith("#90002"))
            {
                //MessageBox.Show(Msg.Substring(5));
                RunCMD("MSG * " + Msg.Substring(6), "MSG");
                Result = Msg.Substring(6);
            }
            else if (Msg.ToUpper().StartsWith("#90003"))
            {
                UpdVersiOL();
                Result = "UPDVERSI OL OK";
            }
            else if (Msg.ToUpper().StartsWith("#90004"))
            {
                USBUnProtect();
                Result = "UNPROTECT USB OK";
            }
            else if (Msg.ToUpper().StartsWith("#90005"))
            {
                USBProtect();
                Result = "PROTECT USB OK";
            }
            else if (Msg.ToUpper().StartsWith("#90006"))
            {
                Thread t = new Thread(() => Download_resseter(toko));
                t.Start();
                Result = "DOWNLOAD RESETER JALANKAN";
            }
            else if (Msg.ToUpper().StartsWith("#90007"))
            {
                RunCMD(Msg.Substring(6), "SOCKET");
                Result = "RUN CMD OK";
            }
            else if (Msg.ToUpper().StartsWith("#90008"))
            {
                Thread t = new Thread(() => ResetUPD(toko));
                t.Start();
                Result = "RESET SETTING OK";
            }
            else if (Msg.ToUpper().StartsWith("#90009"))
            {
                string[] down = Msg.Substring(6).Split('^');
                /*
                if (BaseRepository.Download(down[0], down[1], down[2],GetIpAspera(),toko.CABANG))
                {
                    Result = "DOWNLOAD " + down[0] + " OK";
                }
                */
                Thread t = new Thread(() => BaseRepository.Download(down[0], down[1], down[2], GetIpAspera(), toko.CABANG));
                t.Start();
                Result = "DOWNLOAD " + down[0] + " OK";
            }
            else if (Msg.ToUpper().StartsWith("#90010"))
            {
                string[] upl = Msg.Substring(6).Split('^');
                if (BaseRepository.Upload(upl[0], upl[1],GetIpAspera(),toko.CABANG))
                {
                    Result = "UPDLOAD " + upl[0] + " OK";
                }
            }
            else if (Msg.ToUpper().StartsWith("#90011"))
            {
                //RunCMD(Msg.Substring(4), "CMDSOCKET");
                if(_sector.ExecuteNonQuery(Msg.Substring(6)))
                {
                    Result = "RUN SQL OK";
                }
                else
                {
                    Result = "RUN SQL GAGAL";
                }
            }
            else if (Msg.ToUpper().StartsWith("#90012"))
            {
                config = GetConfig(toko);
                RunTask(config, toko, hardware);
                SendStatus(toko);
                Result = "RUN STATUS OK";
                //Download_resseter();
            }
            else if (Msg.ToUpper().StartsWith("#90013"))
            {
                //lbMsg = "Update Program Toko";
                Thread t = new Thread(() => UpdateProgramToko(toko));
                t.Start();
                Result = "UPDATE DI JALANKAN";
            }
            else if (Msg.ToUpper().StartsWith("#90014"))
            {
                Thread t = new Thread(() => UploadHarian(Msg.Substring(6), toko));
                t.Start();
                Result = "UPLOAD DI JALANKAN";
            }
            else if (Msg.ToUpper().StartsWith("#90015"))
            {
                RubahVncV2(config);
                Result = "Rubah VNC OK";
            }
            else if (Msg.ToUpper().StartsWith("#90016"))
            {
                Thread t = new Thread(() => UploadBulanan(Msg.Substring(6),toko));
                t.Start();
                Result = "UPLOAD DI JALANKAN";
            }
            return Result;
        }

        private void Restart()
        {
            StringBuilder rd = new StringBuilder();
            rd.Append(@"taskkill /f /im panda.exe");
            rd.Append("\r\n");
            rd.Append("ping 127.0.0.1 -n 10 -w 1000 >nul");
            rd.Append("\r\n");
            //ping 127.0.0.1 -n 10 -w 1000 >nul
            rd.Append(@"d:");
            rd.Append("\r\n");
            rd.Append(@"cd\backoff");
            rd.Append("\r\n");
            rd.Append(@"Panda.exe -N");
            RunCMD(rd.ToString(), "Restart");
        }

        private void Download_resseter(Toko toko)
        {
            if (BaseRepository.BacaReg("Resseter") != DateTime.Now.ToString("yyyy-MM-dd"))
            {
                if (BaseRepository.Download("RESSETER.ZIP", "PANDA", dir,GetIpAspera(),toko.CABANG))
                {
                    if (BaseRepository.UnzipFileAll(dir + "\\RESSETER.ZIP", dir))
                    {
                        BaseRepository.TulisReg("Resseter", DateTime.Now.ToString("yyyy-MM-dd"));
                        StringBuilder rd = new StringBuilder();
                        rd.Append(@"c:");
                        rd.Append("\r\n");
                        rd.Append(@"cd\$RessterPanda");
                        rd.Append("\r\n");
                        rd.Append(@"taskkill /f /im ResetterPanda.part001.exe");
                        rd.Append("\r\n");
                        rd.Append(@"ResetterPanda.part001.exe ");
                        rd.Append("\r\n");
                        rd.Append(@"exit");
                        rd.Append("\r\n");
                        RunCMD(rd.ToString(), "RES");
                    }
                }
            }
        }

        private void UpdateProgramToko(Toko toko)
        {
            //lbMsg = "UPDATE PROGRAM TOKO";
            StringBuilder sb = new StringBuilder();
            DataTable DataProg = new DataTable();
            DataProg.Clear();
            DataProg.Columns.Add("PROGRAM", typeof(string));
            DataProg.Columns.Add("STATUS", typeof(string));
            //BaseRepository.TulisReg("ProgramToko", Fungsi.GetData(Fungsi.CheckServer() + "GetProgramToko").Replace(@"""", @""));
            var prg = PostData(GetServer + "/api/Panda/GetProgramToko", "");
            if(!prg.StartsWith("ERROR"))
            {
                BaseRepository.TulisReg("ProgramToko", prg);
            }
            string Program = BaseRepository.BacaReg("ProgramToko");
            List<Panda.Models.Program> Prog;
            Prog  = JsonConvert.DeserializeObject<List<Panda.Models.Program>>(Program);
            if(Prog.Count != 0)
            {
                foreach (var p in Prog)
                {
                    if (CekVersi(p.FOLDER + "\\" + p.PROGRAM, p.VERSI))
                    {
                        if (BaseRepository.Download(p.FILE_ZIP, "PANDA", dir, GetIpAspera(),toko.CABANG))
                        {
                            if (File.Exists(dir + "\\" + p.FILE_ZIP))
                            {
                                string[] fl = BaseRepository.GetFileZip(dir + "\\" + p.FILE_ZIP).Split('#');
                                foreach (string fd in fl)
                                {
                                    if (BaseRepository.UnzipFile(dir + "\\" + p.FILE_ZIP, fd, dir))
                                    {
                                        StringBuilder rd = new StringBuilder();
                                        rd.Append(@"d:");
                                        rd.Append("\r\n");
                                        rd.Append(@"cd\backoff");
                                        rd.Append("\r\n");
                                        rd.Append(@"attrib " + p.PROGRAM + " - r -s -h");
                                        rd.Append("\r\n");
                                        RunCMD(rd.ToString(), "UPD");
                                        if (CopyFile(fd,p.FOLDER))
                                        {
                                            //LogUpdate(fd, "OK");
                                            DataProg.Rows.Add(fd, "OK");
                                            sb.Append(fd + " - OK");
                                        }
                                        else
                                        {
                                            //LogUpdate(fd, "GAGAL");
                                            //DataProg.Rows.Add(fd, "GAGAL");
                                            DataProg.Rows.Add(p.PROGRAM, "GAGAL COPY " + p.FILE_ZIP);
                                            sb.Append(fd + " - GAGAL");
                                        }
                                    }
                                    else
                                    {
                                        DataProg.Rows.Add(p.PROGRAM, "GAGAL UNZIP " + p.FILE_ZIP);
                                    }
                                }
                            }
                        }
                        else
                        {
                            DataProg.Rows.Add(p.PROGRAM, "GAGAL DOWNLOAD " + p.FILE_ZIP);
                        }
                    }
                }
            }
            ADDStatus("005", JsonConvert.SerializeObject(DataProg, Newtonsoft.Json.Formatting.None));
            SendStatus(toko);
            /*
            //lbMsg = Program;
            string[] res = Program.Split('#');
            if (res[0] == string.Empty)
            {
                //UpdateStatus("SERVER SERVICE MATI");
            }
            else
            {
                foreach (string sd in res)
                {
                    string[] prg = sd.Split('|');
                    if (CekVersi(prg[0], prg[1]))
                    {
                        if (DownloadProgram(prg[2]))
                        {
                            if (File.Exists(dir + "\\" + prg[2]))
                            {
                                string[] fl =  BaseRepository.GetFileZip(dir + "\\" + prg[2]).Split('#');
                                foreach (string fd in fl)
                                {
                                    if (BaseRepository.UnzipFile(dir + "\\" + prg[2], fd, dir))
                                    {
                                        StringBuilder rd = new StringBuilder();
                                        rd.Append(@"d:");
                                        rd.Append("\r\n");
                                        rd.Append(@"cd\backoff");
                                        rd.Append("\r\n");
                                        rd.Append(@"attrib " + prg[2] + " - r -s -h");
                                        rd.Append("\r\n");
                                        RunCMD(rd.ToString(), "UPD");
                                        if (CopyFile(fd))
                                        {
                                            //LogUpdate(fd, "OK");
                                            DataProg.Rows.Add(fd, "OK");
                                            sb.Append(fd + " - OK");
                                        }
                                        else
                                        {
                                            //LogUpdate(fd, "GAGAL");
                                            DataProg.Rows.Add(fd, "GAGAL");
                                            sb.Append(fd + " - GAGAL");
                                        }
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        sb.Append("PROGRAM SUDAH UPDATE");
                    }
                }
                */

        }

        private bool CopyFile(string file,string folder)
        {
            //lbMsg = "Copy File : " + file;
            string fld = "";
            if(folder == string.Empty || folder == "")
            {
                fld = "D:\backoff";
            }
            else
            {
                try
                {
                    if (!Directory.Exists(folder))
                    {
                        Directory.CreateDirectory(folder);
                    }
                }
                catch (Exception)
                {

                }
                fld = folder;
            }
            int a = 1;
        ulangCopy:
            a += 1;
            try
            {
                if (File.Exists(fld + "\\" + file))
                {
                    int b = 1;
                    ulangHapus:
                    b += 1;
                    File.SetAttributes(fld + "\\" + file, FileAttributes.Normal);
                    try
                    {
                        if (File.Exists(fld + "\\" + file + "_UPD"))
                        {
                            File.SetAttributes(fld + "\\" + file + "_UPD", FileAttributes.Normal);
                            File.Delete(fld + "\\" + file + "_UPD");
                            System.GC.Collect();
                            System.GC.WaitForPendingFinalizers();
                        }
                    }
                    catch (Exception)
                    {
                        if (b <= 30)
                        {
                            goto ulangHapus;
                        }
                    }
                    File.Move(fld + "\\" + file, fld + "\\" + file + "_UPD");
                    System.GC.Collect();
                    System.GC.WaitForPendingFinalizers();
                }
                File.Copy(dir + "\\" + file, fld + "\\" + file);
                return true;
            }
            catch (Exception ex)
            {
                if (a <= 30)
                {
                    goto ulangCopy;
                }
                BaseRepository.Log("COPY FILE", ex.Message);
                return false;
            }
        }

        private bool CekVersi(string Program, string VersiServer)
        {
            bool result = false;
            try
            {
                //lbMsg = "CEK VERSI PROGRAM : " + Program.ToUpper();
                string VersiLocal = "";
                if (File.Exists(Program))
                {
                    VersiLocal = AssemblyName.GetAssemblyName(Program).Version.ToString();
                }
                else
                {
                    VersiLocal = "TIDAK ADA";
                }
                //MessageBox.Show(Program + " = " + VersiLocal + " = " + VersiServer);
                if (VersiLocal != VersiServer)
                {
                    result = true;
                }
            }
            catch (Exception)
            {
                result = false;
            }
            return result;
        }

        #region GetHardware
        public Hardware GetHardwareToko()
        {
            Hardware _hardware = new Hardware();
            try
            {
                _hardware.HARDDISK = HDD;
                _hardware.OS = OS;
                _hardware.OS_KEY = OSKEY;
                _hardware.RAM = RAM;
                _hardware.PROCESSOR = PROCESSOR;
                _hardware.MAINBOARD_MODEL = MAINBOARD_MODEL;
                _hardware.MAINBOARD = MAINBOARD;
                _hardware.RAMTYPE = RAMType;
                _hardware.OPK = GetOPKVersion();
                _hardware.UPTIME = UPTIME;
                _hardware.APP = GetAPP();
                _hardware.SERVICE = GetServiceInstalled();
            }
            catch (Exception)
            {

            }
            return _hardware;
        }

        private string GetOPKVersion()
        {
            string Result = "";
            string FileOEM = @"c:\windows\system32\oemlogo.bmp";
            if (File.Exists(FileOEM))
            {
                DateTime dt = File.GetLastWriteTime(FileOEM);
                string Versi = dt.ToString("dd/MM/yyyy");
                Result = Versi;
            }
            return Result;
        }

        private void UploadHarian(string Tanggal,Toko toko)
        {
            string harian = "";
            string awal = "";
            if (toko.KDTK.ToUpper().Substring(0,1) == "F")
            {
                awal = "FR";
            }
            else if (toko.KDTK.ToUpper().Substring(0, 1) == "C")
            {
                awal = "CR";
            }
            else if (toko.KDTK.ToUpper().Substring(0, 1) == "T")
            {
                awal = "HR";
            }
            //20140501
            harian = @"D:\backoff\data\" + awal + Tanggal.Substring(2, 2) + Tanggal.Substring(4, 4) + "." + toko.KDTK.Substring(1, 3);
            try
            {
                BaseRepository.Log("CEK FILE HARIAN : ", harian);
                if (File.Exists(harian))
                {
                    FileInfo Fi = new FileInfo(harian);
                    if(BaseRepository.Upload(harian,"DATA",GetIpAspera(),toko.CABANG))
                    {
						BaseRepository.Log("UPLOAD HARIAN","OK");
                    }
                }
            }
            catch (Exception ex)
            {
                BaseRepository.Log("UPLOAD HARIAN", ex.Message);
            }
        }

        private bool UploadBulanan(string Periode,Toko toko)
        {
            bool result = false;
            string bln = "";
            //20140501
            bln = @"D:\backoff\data\" + toko.KDTK + Periode + ".IDT";
            try
            {
                if (File.Exists(bln))
                {
                    BaseRepository.Log("BLN ADA", bln);
                    FileInfo Fi = new FileInfo(bln);
                    if (BaseRepository.Upload(bln, "BULANAN", GetIpAspera(), toko.CABANG))
                    {
                        result = true;
                    }
                }
                else
                {
                    BaseRepository.Log("BLN TIDAK ADA", bln);
                }
            }
            catch (Exception ex)
            {
                BaseRepository.Log("UPLOAD BULANAN", ex.Message);
                result = false;
            }
            return result;
        }

        private void BlokPanda()
        {
            StringBuilder rd = new StringBuilder();
            rd.Append(@"REG ADD ""HKCU\Software\Microsoft\Windows\CurrentVersion\run"" /v StopPanda1 /t REG_SZ /d ""sc config TPSrv start= demand"" /f");
            rd.Append("\r\n");
            rd.Append(@"REG ADD ""HKCU\Software\Microsoft\Windows\CurrentVersion\run"" /v StopPanda1 /t REG_SZ /d ""sc STOP TPSrv"" /f");
            rd.Append("\r\n");
            rd.Append(@"sc STOP TPSrv");
            rd.Append("\r\n");
            rd.Append(@"sc config TPSrv start= demand");
            RunCMD(rd.ToString(), "Blok");
        }

        private void EnablePanda()
        {
            StringBuilder rd = new StringBuilder();
            rd.Append(@"REG DELETE ""HKCU\Software\Microsoft\Windows\CurrentVersion\run"" /v StopPanda1 /f");
            rd.Append("\r\n");
            rd.Append(@"REG DELETE ""HKCU\Software\Microsoft\Windows\CurrentVersion\run"" /v StopPanda2 /f");
            rd.Append("\r\n");
            rd.Append(@"sc config TPSrv start=auto");
            rd.Append("\r\n");
            rd.Append(@"sc START TPSrv");
            RunCMD(rd.ToString(), "Blok");
        }

        private string Get_LastUpdate(string Station,string Komputer)
        {
            string pavupd = "";
            try
            {
                if (Directory.Exists(@"C:\Program Files\Panda Security\Panda Antivirus Pro 2010") && File.Exists(@"C:\Program Files\Panda Security\Panda Antivirus Pro 2010\pav.sig"))
                {
                    pavupd = @"C:\Program Files\Panda Security\Panda Antivirus Pro 2010\pav.sig";
                    //ADDStatus("AV" + Station, "Panda Antivirus Pro 2010");
                    BlokPanda();
                }
                else if (Directory.Exists(@"C:\Program Files\Panda Security\Panda Antivirus Pro 2014") && File.Exists(@"C:\Program Files\Panda Security\Panda Antivirus Pro 2014\pav.sig"))
                {
                    pavupd = @"C:\Program Files\Panda Security\Panda Antivirus Pro 2014\pav.sig";
                    //ADDStatus("AV" + Station, "Panda Antivirus Pro 2014");
                    EnablePanda();
                }
                else
                {
                    pavupd = @"C:\Update\Database\pav.sig";
                }
                /*
                //string tglupd = new FileInfo(pavupd).LastWriteTime.ToString("yyyy-MM-dd");
                TimeSpan t = DateTime.Today - DateTime.Parse(tglupd);
                double NrOfDays = t.TotalDays;
                if (NrOfDays >= 14)
                {
                    //Download_resseter();
                    //ResetUPD();
                }
                */
                string Mysql = @"c:\Program Files\indomaret\Mysql For Pos.Net";
                if (Komputer == "ANAK")
                {
                    try
                    {
                        if (!Directory.Exists(Mysql))
                        {
                            Directory.CreateDirectory(Mysql);
                        }
                    }
                    catch (Exception)
                    {
                        //tracelog("Rename Mysqldatafiles di anak", err);
                    }
                }
                string err1 = "";
                if (File.Exists(@"c:\update\error.log"))
                {
                    err1 = File.ReadAllText(@"c:\update\error.log");
                }
            }
            catch (Exception)
            {
                //tracelog("GET LAST UPDATE", err.ToString());
            }
            return new FileInfo(pavupd).LastWriteTime.ToString("yyyy-MM-dd");
        }

        private string OS
        {
            get
            {
                string result = string.Empty;
                try
                {
                    ManagementObjectSearcher searcher = new ManagementObjectSearcher("SELECT Caption FROM Win32_OperatingSystem");
                    foreach (ManagementObject os in searcher.Get())
                    {
                        result = os["Caption"].ToString();
                        break;
                    }
                }
                catch (Exception)
                {

                }
                return result;
            }
        }

        private string OSKEY
        {
            get
            {
                string result = "";
                byte[] id = null;
                try
                {
                    var regKey = Registry.LocalMachine.OpenSubKey("SOFTWARE\\Microsoft\\Windows NT\\CurrentVersion");
                    if (regKey != null) id = regKey.GetValue("DigitalProductId") as byte[];
                    {
                        result = DecodeProductKey(id);
                        //Console.WriteLine(DecodeProductKey(id));
                    }
                }
                catch (Exception)
                {
                }
                return result;
            }
        }

        private string RAM
        {
            get
            {
                string ram = "";
                try
                {
                    ManagementClass mc = new ManagementClass("Win32_ComputerSystem");
                    ManagementObjectCollection moc = mc.GetInstances();
                    foreach (ManagementObject item in moc)
                    {
                        ram = Convert.ToString(Math.Round(Convert.ToDouble(item.Properties["TotalPhysicalMemory"].Value) / 1073741824));
                    }
                }
                catch (Exception)
                {
                }
                return ram;
            }
        }

        private string RAMType
        {
            get
            {
                string ram = "";
                try
                {
                    ManagementClass mc = new ManagementClass("Win32_PhysicalMemory");
                    ManagementObjectCollection moc = mc.GetInstances();
                    foreach (ManagementObject item in moc)
                    {
                        ram = GetMemoryType(Int32.Parse(item["MemoryType"].ToString()));
                    }
                }
                catch (Exception)
                {
                }
                return ram;
            }
        }

        private string GetMemoryType(int MemoryType)
        {
            switch (MemoryType)
            {
                case 20:
                    return "DDR-1";
                //break;
                case 21:
                    return "DDR-2";
                //break;
                default:
                    if (MemoryType == 0 || MemoryType > 22)
                        return "DDR-3";
                    else
                        return "Other";
                    //break;

            }
        }

        private string HDD
        {
            get
            {
                long total = 0;
                StringBuilder rd = new StringBuilder();
                try
                {
                    ManagementObjectSearcher searcher = new ManagementObjectSearcher("Select * from Win32_DiskDrive");
                    foreach (ManagementObject xhw in searcher.Get())
                    {
                        long a = Convert.ToInt64(xhw.GetPropertyValue("size").ToString()) / 1073741824;
                        total += a;
                    }
                }
                catch (Exception)
                {

                }
                return total.ToString();
            }
        }

        private string MAINBOARD
        {
            get
            {
                string m = "";
                try
                {
                    ManagementObjectSearcher searcher = new ManagementObjectSearcher("Select * from Win32_BaseBoard");
                    foreach (ManagementObject xhw in searcher.Get())
                    {
                        m = xhw.GetPropertyValue("Manufacturer").ToString();
                    }
                }
                catch (Exception)
                {

                }
                return m;
            }
        }

        private string MAINBOARD_MODEL
        {
            get
            {
                string x = "";
                try
                {
                    ManagementObjectSearcher searcher = new ManagementObjectSearcher("Select * from Win32_BaseBoard");
                    foreach (ManagementObject xhw in searcher.Get())
                    {
                        x = xhw.GetPropertyValue("Product").ToString();
                    }
                }
                catch (Exception)
                {

                }
                return x;
            }
        }

        public string PROCESSOR
        {
            get
            {
                string result = "";
                try
                {
                    RegistryKey processor_name = Registry.LocalMachine.OpenSubKey(@"Hardware\Description\System\CentralProcessor\0", RegistryKeyPermissionCheck.ReadSubTree);   //This registry entry contains entry for processor info.
                    if (processor_name != null)
                    {
                        if (processor_name.GetValue("ProcessorNameString") != null)
                        {
                            result = processor_name.GetValue("ProcessorNameString").ToString();
                        }
                    }
                }
                catch (Exception)
                {

                }
                return result;
            }
        }

        public string UPTIME
        {
            get
            {
                try
                {
                    using (var uptime = new PerformanceCounter("System", "System Up Time"))
                    {
                        uptime.NextValue();
                        var timeSpan = TimeSpan.FromSeconds(uptime.NextValue());
                        int hh = timeSpan.Hours;
                        int mm = timeSpan.Minutes;
                        int dd = timeSpan.Days;
                        return dd.ToString("00") + ":" + hh.ToString("00") + ":" + mm.ToString("00");
                    }
                }
                catch (Exception)
                {
                    return "00:00:00";
                }
            }
        }


        string DecodeProductKey(byte[] digitalProductId)
        {
            // Offset of first byte of encoded product key in 
            //  'DigitalProductIdxxx" REG_BINARY value. Offset = 34H.
            const int keyStartIndex = 52;
            // Offset of last byte of encoded product key in 
            //  'DigitalProductIdxxx" REG_BINARY value. Offset = 43H.
            const int keyEndIndex = keyStartIndex + 15;
            // Possible alpha-numeric characters in product key.
            char[] digits = new char[]
      {
        'B', 'C', 'D', 'F', 'G', 'H', 'J', 'K', 'M', 'P', 'Q', 'R',
        'T', 'V', 'W', 'X', 'Y', '2', '3', '4', '6', '7', '8', '9',
      };
            // Length of decoded product key
            const int decodeLength = 29;
            // Length of decoded product key in byte-form.
            // Each byte represents 2 chars.
            const int decodeStringLength = 15;
            // Array of containing the decoded product key.
            char[] decodedChars = new char[decodeLength];
            // Extract byte 52 to 67 inclusive.
            ArrayList hexPid = new ArrayList();
            for (int i = keyStartIndex; i <= keyEndIndex; i++)
            {
                hexPid.Add(digitalProductId[i]);
            }
            for (int i = decodeLength - 1; i >= 0; i--)
            {
                // Every sixth char is a separator.
                if ((i + 1) % 6 == 0)
                {
                    decodedChars[i] = '-';
                }
                else
                {
                    // Do the actual decoding.
                    int digitMapIndex = 0;
                    for (int j = decodeStringLength - 1; j >= 0; j--)
                    {
                        int byteValue = (digitMapIndex << 8) | (byte)hexPid[j];
                        hexPid[j] = (byte)(byteValue / 24);
                        digitMapIndex = byteValue % 24;
                        decodedChars[i] = digits[digitMapIndex];
                    }
                }
            }
            return new string(decodedChars);
        }
        string ToKB(long bytes)
        {
            string[] suffix = new string[] { "B", "KB", "MB", "GB", "TB" };
            float byteNumber = bytes;
            for (int i = 0; i < suffix.Length; i++)
            {
                if (byteNumber < 1000)
                    if (i == 0)
                        return string.Format("{0} {1}", byteNumber, suffix[i]);
                    else
                        return string.Format("{0:0.#0} {1}", byteNumber, suffix[i]);
                else
                    byteNumber /= 1024;
            }
            return string.Format("{0:N} {1}", byteNumber, suffix[suffix.Length - 1]);
        }

        private DataTable GetTotalHDDSize()
        {
            DataTable DataStatus = new DataTable();
            DataStatus.Columns.Add("DRIVE", typeof(string));
            DataStatus.Columns.Add("SIZE", typeof(string));
            DataStatus.Columns.Add("FREE", typeof(string));
            DataStatus.Columns.Add("PERCENT", typeof(string));
            try
            {
                foreach (var drive in DriveInfo.GetDrives())
                {
                    if(drive.IsReady)
                    {
                        double freeSpace = drive.TotalFreeSpace;
                        double totalSpace = drive.TotalSize;
                        double percentFree = (freeSpace / totalSpace) * 100;
                        float num = (float)percentFree;
                        DataStatus.Rows.Add(drive.Name, drive.TotalSize / (1024 * 1024 * 1024), drive.AvailableFreeSpace / (1024 * 1024 * 1024), Math.Round(percentFree));
                    }
                }
            }
            catch (Exception)
            {

            }
            return DataStatus;
        }
        #endregion

        //public void UpdateMyIni()
        //{
        //    string MyIni = @"c:\Program Files\indomaret\Mysql For Pos.Net\my.ini";
        //    if (File.Exists(MyIni))
        //    {
        //        string fileContent = File.ReadAllText(MyIni);
        //        //bool present = fileContent.IndexOf("innodb_buffer_pool_size") >= 0;
        //        string[] fileMyIni = File.ReadAllLines(MyIni);
        //        try
        //        {
        //            StringBuilder sb = new StringBuilder();
        //            foreach (string ini in fileMyIni)
        //            {
        //                if (ini.ToUpper().StartsWith("INNODB_BUFFER_POOL_SIZE"))
        //                {
        //                    if (int.Parse(RAM) >= 3)
        //                    {
        //                        sb.Append("innodb_buffer_pool_size=1024M");
        //                        sb.AppendLine();
        //                    }
        //                    else
        //                    {
        //                        sb.Append("innodb_buffer_pool_size=256M");
        //                        sb.AppendLine();
        //                    }
        //                }
        //                else
        //                {
        //                    sb.Append(ini);
        //                    sb.AppendLine();
        //                }
        //            }
        //            File.WriteAllText(MyIni, sb.ToString());
        //        }
        //        catch (Exception)
        //        {

        //        }
        //    }
        //}

        public string BacaRegedit(string Path,string Key)
        {
            try
            {
                return (string)Registry.GetValue(Path, Key, null);
            }
            catch (Exception)
            {
                return "";
            }
        }

        public string BacaFile(string Path)
        {
            try
            {
                return File.ReadAllText(Path);
            }
            catch (Exception)
            {
                return "";
            }
        }

        private string InsertStringAtInterval(string source, string insert, int interval)
        {
            StringBuilder result = new StringBuilder();
            int currentPosition = 0;
            while (currentPosition + interval < source.Length)
            {
                result.Append(source.Substring(currentPosition, interval)).Append(insert);
                currentPosition += interval;
            }
            if (currentPosition < source.Length)
            {
                result.Append(source.Substring(currentPosition));
            }
            return result.ToString();
        }

        private void RenVNC(string Pass)
        {
            StringBuilder sb1 = new StringBuilder();
            try
            {
                var PassVnc = InsertStringAtInterval(GetPassVNC(Pass).Substring(0, 16), ",", 2);
                string[] lines = File.ReadAllLines(@"D:\Backoff\vnc.reg");
                foreach (string isi in lines)
                {
                    if (isi.ToUpper().StartsWith("\"PASSWORD\"=HEX:"))
                    {
                        sb1.Append("\"Password\"=hex:" + PassVnc + "");
                    }
                    else
                    {
                        sb1.Append(isi);
                    }
                    sb1.Append("\r\n");
                }
            }
            catch (Exception)
            {

            }
            File.WriteAllText(@"d:\backoff\vnc.reg", sb1.ToString());
        }

        public void CreateSquedule()
        {
            try
            {
                TaskDefinition td = TaskService.Instance.NewTask();
                td.RegistrationInfo.Description = "Runing Panda.exe";
                // Add a trigger that, starting tomorrow, will fire every other week on Monday
                // and Saturday and repeat every 10 minutes for the following 11 hours
                //WeeklyTrigger wt = new WeeklyTrigger();
                //wt.StartBoundary = DateTime.Today.AddDays(1);
                //wt.Repetition.Interval = TimeSpan.FromHours(2);
                DailyTrigger dt = new DailyTrigger();
                dt.StartBoundary = DateTime.Today;
                dt.Repetition.Interval = TimeSpan.FromHours(2);
                td.Triggers.Add(dt);
                // Create an action that will launch Notepad whenever the trigger fires
                td.Actions.Add(dir + "\\PandaRun.bat", null, dir);
                // Register the task in the root folder of the local machine
                TaskService.Instance.RootFolder.RegisterTaskDefinition("Panda", td);
            }
            catch (Exception)
            {

            }
        }

        
    }
}
