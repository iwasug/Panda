﻿using Microsoft.Win32;
using MySql.Data.MySqlClient;
using System;
using System.Data;
using System.IO;
using System.Security.Cryptography;
using System.Text;

namespace Panda.Repository
{
    public class SectorRepository : ISector
    {
        //private string SQL = "";
        private MySqlCommand CMD;
        private string GenerateMD5(string SourceText)
        {
            MD5CryptoServiceProvider mD5CryptoServiceProvider = new MD5CryptoServiceProvider();
            byte[] bytes = Encoding.UTF8.GetBytes(SourceText);
            byte[] array = mD5CryptoServiceProvider.ComputeHash(bytes);
            mD5CryptoServiceProvider.Clear();
            int arg_2B_0 = 0;
            checked
            {
                int num = array.Length - 1;
                string text = "";
                for (int i = arg_2B_0; i <= num; i++)
                {
                    text += array[i].ToString("x").PadLeft(2, '0');
                }
                return text;
            }
        }

        private string Decrypt(string cipherText, string passPhrase, string saltValue)
        {
            string strHashName = "SHA1";
            int iterations = 2;
            string s = "@1B2c3D4e5F6g7H8";
            int num = 256;
            byte[] bytes = Encoding.ASCII.GetBytes(s);
            byte[] bytes2 = Encoding.ASCII.GetBytes(saltValue);
            byte[] array = Convert.FromBase64String(cipherText);
            PasswordDeriveBytes passwordDeriveBytes = new PasswordDeriveBytes(passPhrase, bytes2, strHashName, iterations);
            checked
            {
                byte[] bytes3 = passwordDeriveBytes.GetBytes((int)Math.Round((double)num / 8.0));
                ICryptoTransform transform = new RijndaelManaged
                {
                    Mode = CipherMode.CBC
                }.CreateDecryptor(bytes3, bytes);
                MemoryStream memoryStream = new MemoryStream(array);
                CryptoStream cryptoStream = new CryptoStream(memoryStream, transform, CryptoStreamMode.Read);
                byte[] array2 = new byte[array.Length + 1];
                int count = cryptoStream.Read(array2, 0, array2.Length);
                memoryStream.Close();
                cryptoStream.Close();
                return Encoding.UTF8.GetString(array2, 0, count);
            }
        }

        private string Encrypt(string plainText, string passPhrase, string saltValue)
        {
            string strHashName = "SHA1";
            int iterations = 2;
            string s = "@1B2c3D4e5F6g7H8";
            int num = 256;
            byte[] bytes = Encoding.ASCII.GetBytes(s);
            byte[] bytes2 = Encoding.ASCII.GetBytes(saltValue);
            byte[] bytes3 = Encoding.UTF8.GetBytes(plainText);
            PasswordDeriveBytes passwordDeriveBytes = new PasswordDeriveBytes(passPhrase, bytes2, strHashName, iterations);
            byte[] bytes4 = passwordDeriveBytes.GetBytes(checked((int)Math.Round((double)num / 8.0)));
            ICryptoTransform transform = new RijndaelManaged
            {
                Mode = CipherMode.CBC
            }.CreateEncryptor(bytes4, bytes);
            MemoryStream memoryStream = new MemoryStream();
            CryptoStream cryptoStream = new CryptoStream(memoryStream, transform, CryptoStreamMode.Write);
            cryptoStream.Write(bytes3, 0, bytes3.Length);
            cryptoStream.FlushFinalBlock();
            byte[] inArray = memoryStream.ToArray();
            memoryStream.Close();
            cryptoStream.Close();
            return Convert.ToBase64String(inArray);
        }

        private string Versi(string Periode, string User)
        {
            string text = Encrypt(User + " : " + DefaultVer(User), DateTime.Parse(Periode).ToString("yyyy-MM-dd"), User);
            text = text.Replace("'", "");
            return text.Substring(10) + text.Substring(0, 10);
        }
        private string DefaultVer(string User)
        {
            string result = "";
            if (User.ToUpper().Trim() == "root".ToUpper().Trim())
            {
                result = this.Decrypt("AYBOzt4YQ4zHbTY2bRiajA==", User, "12345");
            }
            if (User.ToUpper().Trim() == "kasir".ToUpper().Trim())
            {
                result = this.Decrypt("kRUXVE+bgdwh3Ptfbiw9yg==", User, "12345");
            }
            if (User.ToUpper().Trim() == "app".ToUpper().Trim())
            {
                result = this.Decrypt("hEUSKSjtKQ8dgPIwNIU2Dg==", User, "12345");
            }
            if (User.ToUpper().Trim() == "edp".ToUpper().Trim())
            {
                result = this.Decrypt("21TRmBPTF5Vs2b3mM6FnrA==", User, "12345");
            }
            if (User.ToUpper().Trim() == "dbe".ToUpper().Trim())
            {
                result = this.Decrypt("hGBKT3Q+fgNe9sE4lU2Osw==", User, "12345");
            }
            return result;
        }

        private string GetVersi(string User)
        {
            string Periode = GetPeriode();
            //BaseRepository.Log("VERSI", Periode);
            return Versi(Periode.Substring(0, 4) + "-" + Periode.Substring(4, 2) + "-" + Periode.Substring(6, 2), User.ToLower());
        }

        private string GetVersiV2(string User)
        {
            string Periode = GetPeriodeV2();
            return Versi(Periode.Substring(0, 4) + "-" + Periode.Substring(4, 2) + "-" + Periode.Substring(6, 2), User.ToLower());
        }

        private string GetPeriode()
        {
            string Periode = "";
            try
            {
                if (File.Exists(@"D:\BACKOFF\IDM.SECTOR.TXT"))
                {
                    Periode = File.ReadAllText(@"D:\BACKOFF\IDM.SECTOR.TXT");
                }
                if (Periode.Length != 8)
                {
                    if (File.Exists(@"D:\IDM\IDM.SECTOR.TXT"))
                    {
                        Periode = File.ReadAllText(@"D:\IDM\IDM.SECTOR.TXT");
                    }
                }
            }
            catch (Exception)
            {

            }
            return Periode;
        }

        private string GetPeriodeV2()
        {
            string Periode = "";
            MySqlCommand cmd;
            using (MySqlConnection db = GetDB("dbe"))
            {
                cmd = new MySqlCommand("SELECT `DESC` FROM CONST WHERE RKEY='TG1';",db);
                Periode = cmd.ExecuteScalar().ToString();
            }
            return Periode;
        }

        private MySqlConnection GetDB(string User)
        {
            int x = 1;
        Cobalagi:
            x += 1;
            string ip = (string)Registry.GetValue(@"HKEY_CURRENT_USER\Software\Indomaret\POS.NET\Database", "Server", null);
            string port = (string)Registry.GetValue(@"HKEY_CURRENT_USER\Software\Indomaret\POS.NET\Database", "Port", null);
            string namadb = (string)Registry.GetValue(@"HKEY_CURRENT_USER\Software\Indomaret\POS.NET\Database", "Namadb", null);
            if (namadb == null || namadb == String.Empty)
            {
                namadb = "POS";
            }
            string Server = "";
            string Pass = "";
            if (User.ToUpper() == "EDP")
            {
                Pass = "cUm4l!h4t@datA";
                Server = "allow user variables=true;Persist Security Info=True;server=" + ip + ";userid=" + User.ToLower() + ";password=" + Pass + ";database=" + namadb + ";port=" + port + ";pooling=false;connection timeout=15;Respect Binary flags=false";
            }
            else if (User.ToUpper() == "ROOT" || User.ToUpper() == "KASIR")
            {
                Pass = GetVersi(User);
                Server = "allow user variables=true;Persist Security Info=True;server=" + ip + ";userid=" + User.ToLower() + ";password=" + Pass + ";database=" + namadb + ";port=" + port + ";pooling=false;connection timeout=15;Respect Binary flags=false";
            }
            else if (User.ToUpper() == "DBE")
            {
                Pass = "m@t42_b4roe";
                Server = "allow user variables=true;Persist Security Info=True;server=" + ip + ";userid=" + User.ToLower() + ";password=" + Pass + ";database=dbl;port=" + port + ";pooling=false;connection timeout=15;Respect Binary flags=false";
            }
            MySqlConnection db = new MySqlConnection(Server);
            try
            {
                db.Open();
                //BaseRepository.Log("GET KONEKSI", "KONEK DENGAN USER : " + User);
            }
            catch (Exception)
            {
                //BaseRepository.Log("GET KONEKSI", "Coba Koneksi MySQL " + x.ToString());
                if (x < 6)
                {
                    goto Cobalagi;
                }
                db = null;
            }
            finally
            {
                
            }
            return db;
        }

        private MySqlConnection GetKoneksi()
        {
            //BaseRepository.Log("GET KONEKSI","Coba Koneksi MySQL Root");
            MySqlConnection db1;
            using (MySqlConnection db = GetDB("root"))
            {
                db1 = db;
            }
            if (db1 == null)
            {
                //BaseRepository.Log("GET KONEKSI", "Coba Koneksi MySQL Root Gagal");
                using (MySqlConnection db = GetDB("kasir"))
                {
                    db1 = db;
                }
            }
            if (db1 == null)
            {
                //BaseRepository.Log("GET KONEKSI", "Coba Koneksi MySQL Kasir Gagal");
                using (MySqlConnection db = GetDB("edp"))
                {
                    db1 = db;
                }
            }
            return db1;
        }

        public string ExecuteScalar(string SQL)
        {
            try
            {
                using (MySqlConnection db = GetKoneksi())
                {
                    if (db.State != ConnectionState.Open)
                    {
                        db.Open();
                    }
                    CMD = new MySqlCommand("SET SESSION group_concat_max_len = 10000000;", db);
                    CMD.ExecuteNonQuery();
                    CMD = new MySqlCommand(SQL, db);
                    return CMD.ExecuteScalar().ToString();
                }
            }
            catch (Exception ex)
            {
                BaseRepository.Log("GET KONEKSI", ex.Message);
                return "GAGAL";
            }
        }

        public bool ExecuteNonQuery(string SQL)
        {
            try
            {
                using (MySqlConnection db = GetKoneksi())
                {
                    if (db.State != ConnectionState.Open)
                    {
                        db.Open();
                    }
                    CMD = new MySqlCommand(SQL, db);
                    CMD.ExecuteNonQuery();
                    return true;
                }
            }
            catch (Exception ex)
            {
                BaseRepository.Log("EKSEKUSI QUERY", ex.Message);
                return false;
            }
        }

        public DataTable GetDataTable(string SQL)
        {
            DataTable dt = new DataTable();
            try
            {
                using (MySqlConnection db = GetKoneksi())
                {
                    if (db.State != ConnectionState.Open)
                    {
                        db.Open();
                    }
                    CMD = new MySqlCommand(SQL, db);
                    MySqlDataAdapter da = new MySqlDataAdapter(CMD);
                    da.Fill(dt);
                }
            }
            catch (Exception ex)
            {
                BaseRepository.Log("GET DATATABLE", ex.Message);
            }
            return dt;
        }
    }
}
