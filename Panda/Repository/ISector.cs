﻿using System.Data;

namespace Panda.Repository
{
    interface ISector
    {
        string ExecuteScalar(string SQL);
        bool ExecuteNonQuery(string SQL);
        DataTable GetDataTable(string SQL);
    }
}
