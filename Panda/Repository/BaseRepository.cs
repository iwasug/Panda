﻿using Ionic.Zip;
using Ionic.Zlib;
using Microsoft.Win32;
using System;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using System.Reflection;
using System.Text;
using Tamir.SharpSsh;

namespace Panda.Repository
{
    public static class BaseRepository
    {
        public static void Log(string Proses, string Message)
        {
            StreamWriter sw = null;
            string log = AppDomain.CurrentDomain.BaseDirectory + "\\Panda.log";
            try
            {
                if (File.Exists(log))
                {
                    long length = new FileInfo(log).Length;
                    if (length >= 5485760)
                    {
                        File.Delete(log);
                    }
                }
                sw = new StreamWriter(log, true);
                sw.WriteLine(DateTime.Now.ToString() + " #### [ " + GetIP() + " ] - " + Proses + "  #### #### " + Message + " ####");
                sw.Flush();
                sw.Close();
            }
            catch
            {

            }
        }

        private static string GetIP()
        {
            IPHostEntry host;
            string localIP = "";
            host = Dns.GetHostEntry(Dns.GetHostName());
            foreach (IPAddress ip in host.AddressList)
            {
                if (ip.AddressFamily == AddressFamily.InterNetwork)
                {
                    localIP = ip.ToString();
                    break;
                }
            }
            return localIP;
        }

        static RegistryKey baseRegistryKey = Registry.CurrentUser;
        private static string subKey = "SOFTWARE\\Panda";
        public static bool TulisReg(string KeyName, object Value)
        {
            try
            {
                RegistryKey rk = baseRegistryKey;
                RegistryKey sk1 = rk.CreateSubKey(subKey);
                sk1.SetValue(KeyName, Value);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
        public static string BacaReg(string KeyName)
        {
            RegistryKey rk = baseRegistryKey;
            RegistryKey sk1 = rk.OpenSubKey(subKey);
            if (sk1 == null)
            {
                return null;
            }
            else
            {
                try
                {
                    return (string)sk1.GetValue(KeyName);
                }
                catch (Exception)
                {
                    return null;
                }
            }
        }

        public static bool ping(string ip)
        {
            bool sts = false;
            int timeout = 5000;
            try
            {
                if (ip == "")
                {
                    sts = false;
                }
                else
                {
                    Ping pingSender = new Ping();
                    PingReply reply = pingSender.Send(ip, timeout);
                    if (reply.Status == IPStatus.Success)
                    {
                        sts = true;
                    }
                    else
                    {
                        sts = false;
                    }
                }
            }
            catch (Exception)
            {

            }
            return sts;
        }

        public static bool GetFileRes(string NamaFile, string NamaFileAs, string ToFolder)
        {
            try
            {
                Assembly Asm = Assembly.GetExecutingAssembly();
                Stream strm = Asm.GetManifestResourceStream("Panda.Files." + NamaFile);
                BinaryReader br = new BinaryReader(strm);
                FileStream fs = File.Create(ToFolder + "\\" + NamaFileAs);
                BinaryWriter bw = new BinaryWriter(fs);
                bw.Write(br.ReadBytes(checked((int)br.BaseStream.Length)));
                bw.Flush();
                fs.Close();
                bw.Close();
                return true;
            }
            catch (Exception ex)
            {
                Log("LOAD FILE RES", ex.Message);
                return false;
            }
        }

        public static bool UnzipFileAll(string fileZip, string folder)
        {
            try
            {
                using (ZipFile zip1 = ZipFile.Read(fileZip))
                {
                    if (!Directory.Exists(folder))
                    {
                        Directory.CreateDirectory(folder);
                    }
                    foreach (ZipEntry e in zip1)
                    {
                        e.Extract(folder, ExtractExistingFileAction.OverwriteSilently);
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                Log("UNZIP FILE ALL",ex.Message);
                return false;
            }
        }

        private static Scp sshCp;

        public static bool Download(string xFile, string xFolderFTP, string xFolderLokal, string IpAspera, string Cabang)
        {
            try
            {
                Log("DOWNLOAD MULAI ", "Download " + xFile);
                if (!Directory.Exists(xFolderLokal))
                {
                    Directory.CreateDirectory(xFolderLokal);
                }
                sshCp = new Scp(IpAspera, "ftptoko");
                sshCp.Password = "xftptokox";
                sshCp.Connect();
                sshCp.Get(@"/home/ftp/" + Cabang.ToLower() + "/" + Cabang.ToUpper() + "/" + xFolderFTP + "/" + xFile, xFolderLokal);
                Log("DOWNLOAD SUKSES ","Download " + xFile + " OK");
                return true;
            }
            catch (Exception er)
            {
                //lbMsg = "Download " + xFile + " GAGAL " + er.Message;
                Log("DOWNLOAD GAGAL ", er.Message);
                return false;
            }
            finally
            {
                sshCp.Close();
            }
        }

        public static bool Upload(string xFile, string xFolder, string IpAspera, string Cabang)
        {
            try
            {
                if (File.Exists(xFile))
                {
                    FileInfo Fi = new FileInfo(xFile);
                    sshCp = new Scp(IpAspera, "ftptoko");
                    sshCp.Password = "xftptokox";
                    sshCp.Connect();
                    sshCp.Put(xFile, @"/home/ftp/" + Cabang.ToLower() + "/" + Cabang.ToUpper() + "/" + xFolder + "/" + Fi.Name);
                    Log("UPLOAD SUKSES ", "Upload " + xFile + " OK");
                }
                return true;
            }
            catch (Exception er)
            {
                Log("UPLOAD GAGAL ", er.Message);
                return false;
            }
            finally
            {
                sshCp.Close();
            }
        }

        //private static DataTable _DataStatus;
        public static DataTable DataStatus;
        public static string ZipString(string value)
        {
            byte[] buffer = Encoding.UTF8.GetBytes(value);
            var memoryStream = new MemoryStream();
            using (var gZipStream = new GZipStream(memoryStream, CompressionMode.Compress, true))
            {
                gZipStream.Write(buffer, 0, buffer.Length);
            }
            memoryStream.Position = 0;
            var compressedData = new byte[memoryStream.Length];
            memoryStream.Read(compressedData, 0, compressedData.Length);
            var gZipBuffer = new byte[compressedData.Length + 4];
            Buffer.BlockCopy(compressedData, 0, gZipBuffer, 4, compressedData.Length);
            Buffer.BlockCopy(BitConverter.GetBytes(buffer.Length), 0, gZipBuffer, 0, 4);
            return Convert.ToBase64String(gZipBuffer);
        }

        public static string UnZipString(string value)
        {
            byte[] gZipBuffer = Convert.FromBase64String(value);
            using (var memoryStream = new MemoryStream())
            {
                int dataLength = BitConverter.ToInt32(gZipBuffer, 0);
                memoryStream.Write(gZipBuffer, 4, gZipBuffer.Length - 4);
                var buffer = new byte[dataLength];
                memoryStream.Position = 0;
                using (var gZipStream = new GZipStream(memoryStream, CompressionMode.Decompress))
                {
                    gZipStream.Read(buffer, 0, buffer.Length);
                }
                return Encoding.UTF8.GetString(buffer);
            }
        }

        public static string GetFileZip(string FileZip)
        {
            string result = "";
            try
            {
                using (ZipFile zip = ZipFile.Read(FileZip))
                {
                    int a = 0;
                    foreach (ZipEntry e in zip)
                    {
                        a += 1;
                        if (a != zip.Count)
                        {
                            result += e.FileName + "#";
                        }
                        else
                        {
                            result += e.FileName;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Log("GET FILE ZIP",ex.Message);
            }
            return result;
        }

        public static bool Show;

        public static bool UnzipFile(string fileZip, string file, string folder)
        {
            try
            {
                using (ZipFile zip = ZipFile.Read(fileZip))
                {
                    ZipEntry e = zip[file];
                    e.Extract(folder, ExtractExistingFileAction.OverwriteSilently);
                }
                return true;
            }
            catch (Exception ex)
            {
                Log("UNZIP FILE",ex.ToString() + " - " + fileZip + " - " + file);
                return false;
            }
        }

        public static void Kunci()
        {
            try
            {
                Registry.SetValue("HKEY_LOCAL_MACHINE\\Software\\Microsoft\\Windows\\CurrentVersion\\Policies\\ActiveDesktop", "NoChangingWallpaper", 1);
                Registry.SetValue("HKEY_LOCAL_MACHINE\\SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\policies\\Explorer", "NoActiveDesktop", 1);
                Registry.SetValue("HKEY_LOCAL_MACHINE\\SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\policies\\Explorer", "NoActiveDesktopChanges", 1);
                Registry.SetValue("HKEY_LOCAL_MACHINE\\SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\policies\\Explorer", "NoThemesTab", 1);
                Registry.SetValue("HKEY_LOCAL_MACHINE\\SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\policies\\Explorer", "NoChangeAnimation", 1);
                Registry.SetValue("HKEY_LOCAL_MACHINE\\SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\policies\\Explorer", "NoRun", 1);
                Registry.SetValue("HKEY_LOCAL_MACHINE\\SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\policies\\Explorer", "NoViewContextMenu", 1);
                Registry.SetValue("HKEY_LOCAL_MACHINE\\SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\policies\\Explorer", "NoFind", 1);
                Registry.SetValue("HKEY_LOCAL_MACHINE\\SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\policies\\Explorer", "NoSMHelp", 1);
                Registry.SetValue("HKEY_LOCAL_MACHINE\\SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\policies\\Explorer", "NoSMConfigurePrograms", 1);
                Registry.SetValue("HKEY_LOCAL_MACHINE\\SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\policies\\Explorer", "NoWindowsUpdate", 1);
                Registry.SetValue("HKEY_LOCAL_MACHINE\\SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\policies\\Explorer", "NoSMMyDocs", 1);
                Registry.SetValue("HKEY_LOCAL_MACHINE\\SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\policies\\Explorer", "NoRecentDocsHistory", 1);
                Registry.SetValue("HKEY_LOCAL_MACHINE\\SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\policies\\Explorer", "NoSMMyPictures", 1);
                Registry.SetValue("HKEY_LOCAL_MACHINE\\SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\policies\\Explorer", "NoRecentDocsMenu", 1);
                Registry.SetValue("HKEY_LOCAL_MACHINE\\SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\policies\\Explorer", "NoNetworkConnections", 1);
                Registry.SetValue("HKEY_LOCAL_MACHINE\\SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\policies\\Explorer", "NoSetTaskbar", 1);
                Registry.SetValue("HKEY_LOCAL_MACHINE\\SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\policies\\Explorer", "NoChangeStartMenu", 1);
                Registry.SetValue("HKEY_LOCAL_MACHINE\\SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\policies\\Explorer", "NoFavoritesMenu", 1);
                Registry.SetValue("HKEY_LOCAL_MACHINE\\SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\policies\\Explorer", "ForceStartMenuLogoff", 1);
                Registry.SetValue("HKEY_LOCAL_MACHINE\\SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\policies\\Explorer", "NoDesktop", 1);
                Registry.SetValue("HKEY_LOCAL_MACHINE\\SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\policies\\Explorer", "NoStartMenuPinnedList", 1);
                Registry.SetValue("HKEY_LOCAL_MACHINE\\SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\policies\\Explorer", "NoStartMenuMFUprogramsList", 1);
                Registry.SetValue("HKEY_LOCAL_MACHINE\\SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\policies\\Explorer", "NoUserNameInStartMenu", 1);
                Registry.SetValue("HKEY_LOCAL_MACHINE\\SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\policies\\Explorer", "NoStartMenuMorePrograms", 1);
                Registry.SetValue("HKEY_LOCAL_MACHINE\\Software\\Microsoft\\Windows\\CurrentVersion\\Policies\\Explorer", "NoViewContextMenu", 1);
                Registry.SetValue("HKEY_CURRENT_USER\\Control Panel\\Desktop", "PaintDesktopVersion", 1);
                Registry.SetValue("HKEY_LOCAL_MACHINE\\Software\\Microsoft\\Windows\\CurrentVersion\\Policies\\Explorer", "NoSMMyDocs", 1);
                Registry.SetValue("HKEY_LOCAL_MACHINE\\Software\\Microsoft\\Windows\\CurrentVersion\\Policies\\Explorer", "NoRecentDocsHistory", 1);
                Registry.SetValue("HKEY_LOCAL_MACHINE\\Software\\Microsoft\\Windows\\CurrentVersion\\Policies\\Explorer", "NoSMMyPictures", 1);
                Registry.SetValue("HKEY_LOCAL_MACHINE\\Software\\Microsoft\\Windows\\CurrentVersion\\Policies\\Explorer", "NoRecentDocsMenu", 1);
                Registry.SetValue("HKEY_LOCAL_MACHINE\\Software\\Microsoft\\Windows\\CurrentVersion\\Policies\\Explorer", "NoSMHelp", 1);
                Registry.SetValue("HKEY_LOCAL_MACHINE\\Software\\Microsoft\\Windows\\CurrentVersion\\Policies\\Explorer", "NoStartBanner", 1);
                Registry.SetValue("HKEY_LOCAL_MACHINE\\Software\\Microsoft\\Windows\\CurrentVersion\\Policies\\Explorer", "NoSetTaskbar", 1);
                Registry.SetValue("HKEY_LOCAL_MACHINE\\Software\\Microsoft\\Windows\\CurrentVersion\\Policies\\Explorer", "NoChangeStartMenu", 1);
                Registry.SetValue("HKEY_LOCAL_MACHINE\\Software\\Microsoft\\Windows\\CurrentVersion\\Policies\\Explorer", "NoRun", 1);
                Registry.SetValue("HKEY_LOCAL_MACHINE\\Software\\Microsoft\\Windows\\CurrentVersion\\Policies\\Explorer", "NoFavoritesMenu", 1);
                Registry.SetValue("HKEY_CURRENT_USER\\Control Panel\\Desktop", "HungAppTimeout", 1000);
                Registry.SetValue("HKEY_CURRENT_USER\\Control Panel\\Desktop", "WaitToKillAppTimeout", 1000);
                Registry.SetValue("HKEY_CURRENT_USER\\Control Panel\\Desktop", "AutoEndTasks", 1);
                Registry.SetValue("HKEY_CURRENT_USER\\Control Panel\\don't load", "timedate.cpl", "No");
                Registry.SetValue("HKEY_CURRENT_USER\\Software\\ORL\\WinVNC3", "BlackAlphaBlending", 1);
                Registry.SetValue("HKEY_CURRENT_USER\\Software\\Microsoft\\Windows\\CurrentVersion\\Policies\\Explorer", "HideClock", 1);
                Registry.SetValue("HKEY_CURRENT_USER\\Software\\Microsoft\\Windows\\CurrentVersion\\Policies\\System", "NoColorChoice", 1);
                Registry.SetValue("HKEY_CURRENT_USER\\Software\\Microsoft\\Windows\\CurrentVersion\\Policies\\System", "NoSizeChoice", 1);
                Registry.SetValue("HKEY_CURRENT_USER\\Software\\Microsoft\\Windows\\CurrentVersion\\Policies\\System", "NoVisualStyleChoice", 1);
                Registry.SetValue("HKEY_CURRENT_USER\\Software\\Microsoft\\Windows\\CurrentVersion\\Policies\\System", "DisableTaskMgr", 1);
                Registry.SetValue("HKEY_CURRENT_USER\\Software\\Microsoft\\Windows\\CurrentVersion\\Policies\\Explorer", "NoTrayContextMenu", 1);
                Registry.SetValue("HKEY_LOCAL_MACHINE\\SYSTEM\\CurrentControlSet\\Services\\USBSTOR", "Type", 1);
                Registry.SetValue("HKEY_LOCAL_MACHINE\\SYSTEM\\CurrentControlSet\\Services\\USBSTOR", "Start", 4);
                Registry.SetValue("HKEY_LOCAL_MACHINE\\SYSTEM\\CurrentControlSet\\Services\\USBSTOR", "ErrorControl", 1);
                Registry.SetValue("HKEY_LOCAL_MACHINE\\SYSTEM\\CurrentControlSet\\Services\\USBSTOR", "DisplayName", "USB Mass Storage Driver");
            }
            catch (Exception)
            {

            }
        }

        public static void BukaKunci()
        {
            Registry.SetValue("HKEY_LOCAL_MACHINE\\Software\\Microsoft\\Windows\\CurrentVersion\\Policies\\ActiveDesktop", "NoChangingWallpaper", 0);
            Registry.SetValue("HKEY_LOCAL_MACHINE\\SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\policies\\Explorer", "NoActiveDesktop", 0);
            Registry.SetValue("HKEY_LOCAL_MACHINE\\SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\policies\\Explorer", "NoActiveDesktopChanges", 0);
            Registry.SetValue("HKEY_LOCAL_MACHINE\\SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\policies\\Explorer", "NoThemesTab", 0);
            Registry.SetValue("HKEY_LOCAL_MACHINE\\SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\policies\\Explorer", "NoChangeAnimation", 0);
            Registry.SetValue("HKEY_LOCAL_MACHINE\\SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\policies\\Explorer", "NoRun", 0);
            Registry.SetValue("HKEY_LOCAL_MACHINE\\SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\policies\\Explorer", "NoViewContextMenu", 0);
            Registry.SetValue("HKEY_LOCAL_MACHINE\\SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\policies\\Explorer", "NoFind", 0);
            Registry.SetValue("HKEY_LOCAL_MACHINE\\SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\policies\\Explorer", "NoSMHelp", 0);
            Registry.SetValue("HKEY_LOCAL_MACHINE\\SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\policies\\Explorer", "NoSMConfigurePrograms", 0);
            Registry.SetValue("HKEY_LOCAL_MACHINE\\SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\policies\\Explorer", "NoWindowsUpdate", 0);
            Registry.SetValue("HKEY_LOCAL_MACHINE\\SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\policies\\Explorer", "NoSMMyDocs", 0);
            Registry.SetValue("HKEY_LOCAL_MACHINE\\SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\policies\\Explorer", "NoRecentDocsHistory", 0);
            Registry.SetValue("HKEY_LOCAL_MACHINE\\SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\policies\\Explorer", "NoSMMyPictures", 0);
            Registry.SetValue("HKEY_LOCAL_MACHINE\\SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\policies\\Explorer", "NoRecentDocsMenu", 0);
            Registry.SetValue("HKEY_LOCAL_MACHINE\\SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\policies\\Explorer", "NoNetworkConnections", 0);
            Registry.SetValue("HKEY_LOCAL_MACHINE\\SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\policies\\Explorer", "NoSetTaskbar", 0);
            Registry.SetValue("HKEY_LOCAL_MACHINE\\SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\policies\\Explorer", "NoChangeStartMenu", 0);
            Registry.SetValue("HKEY_LOCAL_MACHINE\\SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\policies\\Explorer", "NoFavoritesMenu", 0);
            Registry.SetValue("HKEY_LOCAL_MACHINE\\SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\policies\\Explorer", "ForceStartMenuLogoff", 0);
            Registry.SetValue("HKEY_LOCAL_MACHINE\\SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\policies\\Explorer", "NoDesktop", 0);
            Registry.SetValue("HKEY_LOCAL_MACHINE\\SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\policies\\Explorer", "NoStartMenuPinnedList", 0);
            Registry.SetValue("HKEY_LOCAL_MACHINE\\SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\policies\\Explorer", "NoStartMenuMFUprogramsList", 0);
            Registry.SetValue("HKEY_LOCAL_MACHINE\\SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\policies\\Explorer", "NoUserNameInStartMenu", 0);
            Registry.SetValue("HKEY_LOCAL_MACHINE\\SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\policies\\Explorer", "NoStartMenuMorePrograms", 0);
            Registry.SetValue("HKEY_LOCAL_MACHINE\\Software\\Microsoft\\Windows\\CurrentVersion\\Policies\\Explorer", "NoViewContextMenu", 0);
            Registry.SetValue("HKEY_CURRENT_USER\\Control Panel\\Desktop", "PaintDesktopVersion", 0);
            Registry.SetValue("HKEY_LOCAL_MACHINE\\Software\\Microsoft\\Windows\\CurrentVersion\\Policies\\Explorer", "NoSMMyDocs", 0);
            Registry.SetValue("HKEY_LOCAL_MACHINE\\Software\\Microsoft\\Windows\\CurrentVersion\\Policies\\Explorer", "NoRecentDocsHistory", 0);
            Registry.SetValue("HKEY_LOCAL_MACHINE\\Software\\Microsoft\\Windows\\CurrentVersion\\Policies\\Explorer", "NoSMMyPictures", 0);
            Registry.SetValue("HKEY_LOCAL_MACHINE\\Software\\Microsoft\\Windows\\CurrentVersion\\Policies\\Explorer", "NoRecentDocsMenu", 0);
            Registry.SetValue("HKEY_LOCAL_MACHINE\\Software\\Microsoft\\Windows\\CurrentVersion\\Policies\\Explorer", "NoSMHelp", 0);
            Registry.SetValue("HKEY_LOCAL_MACHINE\\Software\\Microsoft\\Windows\\CurrentVersion\\Policies\\Explorer", "NoStartBanner", 0);
            Registry.SetValue("HKEY_LOCAL_MACHINE\\Software\\Microsoft\\Windows\\CurrentVersion\\Policies\\Explorer", "NoSetTaskbar", 0);
            Registry.SetValue("HKEY_LOCAL_MACHINE\\Software\\Microsoft\\Windows\\CurrentVersion\\Policies\\Explorer", "NoChangeStartMenu", 0);
            Registry.SetValue("HKEY_LOCAL_MACHINE\\Software\\Microsoft\\Windows\\CurrentVersion\\Policies\\Explorer", "NoRun", 0);
            Registry.SetValue("HKEY_LOCAL_MACHINE\\Software\\Microsoft\\Windows\\CurrentVersion\\Policies\\Explorer", "NoFavoritesMenu", 0);
            Registry.SetValue("HKEY_CURRENT_USER\\Control Panel\\Desktop", "HungAppTimeout", "1000");
            Registry.SetValue("HKEY_CURRENT_USER\\Control Panel\\Desktop", "WaitToKillAppTimeout", "1000");
            Registry.SetValue("HKEY_CURRENT_USER\\Control Panel\\Desktop", "AutoEndTasks", "1");
            Registry.SetValue("HKEY_CURRENT_USER\\Control Panel\\don't load", "timedate.cpl", "Yes");
            Registry.SetValue("HKEY_CURRENT_USER\\Software\\ORL\\WinVNC3", "BlackAlphaBlending", 0);
            Registry.SetValue("HKEY_CURRENT_USER\\Software\\Microsoft\\Windows\\CurrentVersion\\Policies\\Explorer", "HideClock", 0);
            Registry.SetValue("HKEY_CURRENT_USER\\Software\\Microsoft\\Windows\\CurrentVersion\\Policies\\System", "NoColorChoice", 0);
            Registry.SetValue("HKEY_CURRENT_USER\\Software\\Microsoft\\Windows\\CurrentVersion\\Policies\\System", "NoSizeChoice", 0);
            Registry.SetValue("HKEY_CURRENT_USER\\Software\\Microsoft\\Windows\\CurrentVersion\\ Policies\\System", "NoVisualStyleChoice", 0);
            Registry.SetValue("HKEY_CURRENT_USER\\Software\\Microsoft\\Windows\\CurrentVersion\\Policies\\System", "DisableTaskMgr", 0);
            Registry.SetValue("HKEY_CURRENT_USER\\Software\\Microsoft\\Windows\\CurrentVersion\\Policies\\Explorer", "NoTrayContextMenu", 0);
            Registry.SetValue("HKEY_LOCAL_MACHINE\\SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\policies\\Explorer", "NoControlPanel", 0);
            Registry.SetValue("HKEY_LOCAL_MACHINE\\SYSTEM\\CurrentControlSet\\Services\\USBSTOR", "Start", 3);
            Registry.SetValue("HKEY_CURRENT_USER\\Software\\Policies\\Microsoft\\Windows\\System", "DisableCMD", 0);
        }

        public static void RunExplorer()
        {
            Process proc = new Process();
            int x = 30;
        UlangHapusBat:
            x += 1;
            try
            {
                System.GC.Collect();
                System.GC.WaitForPendingFinalizers();
                if (File.Exists(@"c:\$RessterPanda\CMDEXPLORER.bat"))
                {
                    File.Delete(@"c:\$RessterPanda\CMDEXPLORER.bat");
                }
            }
            catch (Exception)
            {
                if (x < 30)
                {
                    goto UlangHapusBat;
                }
            }
            try
            {
                StringBuilder rd = new StringBuilder();
                rd.Append(@"@ECHO OFF");
                rd.Append("\r\n");
                rd.Append("TSKILL EXPLORER");
                rd.Append("\r\n");
                rd.Append("TSKILL OPKV6*");
                rd.Append("\r\n");
                rd.Append("ping 127.0.0.1 -n 10 -w 1000 >nul");
                rd.Append("\r\n");
                rd.Append("EXPLORER");
                rd.Append("\r\n");
                rd.Append("exit");
                File.WriteAllText(@"c:\$RessterPanda\CMDEXPLORER.bat", rd.ToString());
                proc.EnableRaisingEvents = false;
                proc.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
                proc.StartInfo.FileName = @"c:\$RessterPanda\CMDEXPLORER.bat";
                proc.Start();
            }
            catch (Exception ex)
            {
                Log("RUN CMD", ex.Message);
            }
            finally
            {
                proc.Close();
            }

        }
    }
}
