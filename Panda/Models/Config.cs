﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Panda.Models
{
    public class Config
    {
        public string RECID { set; get; }
        public string JAM { set; get; }
        public string TASK { set; get; }
        //public string SQL { set; get; }
        //public string CMD { set; get; }
        public string IP_ASPERA { set; get; }
        public string IP_PANDA { set; get; }
        public string STATUS_PROGRAM { set; get; }
        public string VNC { set; get; }
        public string AMS { set; get; }
        public string USB { set; get; }
        public string VERSI { set; get; }
    }
}
