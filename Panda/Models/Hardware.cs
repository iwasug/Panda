﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Panda.Models
{
    public class Hardware
    {
        public string PROCESSOR { set; get; }
        public string MAINBOARD { set; get; }
        public string RAM { set; get; }
        public string RAMTYPE { set; get; }
        public string HARDDISK { set; get; }
        public string MAINBOARD_MODEL { set; get; }
        public string OS { set; get; }
        public string OS_KEY { set; get; }
        public string OPK { set; get; }
        public string UPTIME { set; get; }
        public string APP { set; get; }
        public string SERVICE { set; get; }
    }
}
