﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Panda.Models
{
    public class Program
    {
        public string PROGRAM { set; get; }
        public string VERSI { set; get; }
        public string FILE_ZIP { set; get; }
        public string FOLDER { set; get; }
    }
}
