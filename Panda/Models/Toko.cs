﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Panda.Models
{
    public class Toko
    {
        public string KDTK { set; get; }
        public string NAMA { set; get; }
        public string ALAMAT { set; get; }
        public string CABANG { set; get; }
        public string STATION { set; get; }
        public string KOMPUTER { set; get; }
        public string LAST_UPDATE { set; get; }
        public string ERROR { set; get; }
        public string VERSI { set; get; }
        public string ISTC { set; get; }
    }
}
