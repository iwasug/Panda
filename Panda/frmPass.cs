﻿using System;
using System.Windows.Forms;
using Panda.Repository;
namespace Panda
{
    public partial class frmPass : Form
    {
        public frmPass()
        {
            InitializeComponent();
        }
        private static string _toko;
        public string toko
        {
            set { _toko = value; }
            get { return _toko; }
        }
        private static bool _passes;
        public bool passes
        {
            set { _passes = value; }
            get { return _passes; }
        }

        private void cekpass()
        {
            if (txtPass.Text == "IWASUG" || txtPass.Text == Pass() || txtPass.Text == "EDPCRB")
            {
                //fungsi.UnProtectUSB();
                MessageBox.Show("SUDAH BISA DI GUNAKAN", "PANDA", MessageBoxButtons.OK, MessageBoxIcon.Information);
                passes = true;
                BaseRepository.Log("PASS", "BUKA OK " + txtPass.Text);
                this.Close();
            }
            else
            {
                BaseRepository.Log("PASS", "BUKA GAGAL " + txtPass.Text);
                MessageBox.Show("PASSWORD SALAH", "PANDA", MessageBoxButtons.OK, MessageBoxIcon.Error);
                passes = false;
                txtPass.Text = "";
            }
        }

        private string Pass()
        {
            try
            {
                double thn = double.Parse(DateTime.Now.ToString("yyyy"));
                double bln = double.Parse(DateTime.Now.ToString("MM"));
                double tgl = double.Parse(DateTime.Now.ToString("dd"));
                double jam = double.Parse(DateTime.Now.ToString("HH"));
                double hasil = (thn + bln + tgl) * jam;
                return hasil.ToString() + ReverseString(hasil.ToString());
            }
            catch (Exception)
            {
                return "";
            }

        }

        private string ReverseString(string s)
        {
            char[] arr = s.ToCharArray();
            Array.Reverse(arr);
            return new string(arr);
        }

        private void txtPass_MouseDown(object sender, MouseEventArgs e)
        {

        }

        private void txtPass_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                cekpass();
            }
            else if (e.KeyCode == Keys.Escape)
            {
                this.Close();
            }
        }



        private void timer1_Tick(object sender, EventArgs e)
        {
            waktu = waktu - 1;
            if (waktu == 0)
            {
                this.Close();
            }
        }

        int waktu;
        private void mulai()
        {
            waktu = 60;
            timer1.Interval = 1000;
            timer1.Start();
        }

        private void frmPass_Load(object sender, EventArgs e)
        {
            mulai();
        }
    }
}
